
/*global $, jQuery*/
$(document).ready(function () {
    'use strict';

    $('.navBar .navbar-expand-lg .navbar-nav .dropdown-toggle').click(function () {
        $(this).siblings('.navBar .navbar-expand-lg .navbar-nav .dropdown-menu').slideToggle();
    });

    $('.testimonialCarosal').slick({
        dots: true,
        infinite: true,
        speed: 300,
        autoplay: true,
        slidesToShow: 1,
        adaptiveHeight: true
    });


    $('.NewPartner').slick({
        infinite: true,
        autoplay: true,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    arrows: false
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    arrows: false
                }
            },
            {
                breakpoint: 500,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });

    $('.TemporaryDeals').slick({
        infinite: true,
        speed: 300,
        autoplay: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    arrows: false
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    arrows: false
                }
            },
            {
                breakpoint: 500,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
    $(function () {
        $(".MoreItems").click(function (e) {
            $(this).toggleClass("hide");
            $(".MorePartners").toggleClass("open");

            e.preventDefault();

        });
    });
    $(function () {
        $(".MoreCategory").click(function (e) {
            $(this).toggleClass("hide");
            $(".MoreCategories").toggleClass("open");

            e.preventDefault();

        });
        $(".Togary .list-group-item a").click(function () {
            $('html, body').animate({
                scrollTop: 0
            }, 1000);
        });

    });
});