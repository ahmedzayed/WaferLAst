﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using SaveIt.Entity;
using SaveIt.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using SaveIt.Areas.Admin.Controllers;
using SaveIt.ModelView;
using System.Data.Entity.Migrations;

namespace SaveIt.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        SaveItEntities db = new SaveItEntities();
        public HomeController()
        {

        }

        public HomeController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Home()
        {

            ViewBag.Cat = db.Categorys.ToList();
            var List = db.Categorys.Take(6).ToList();
            List<Donator> FirstDontar = new List<Donator>();
            List<Donator> ScDontar = new List<Donator>();
            List<Donator> thrDontar = new List<Donator>();

            var ListDontarfirst = db.Donators.Where(a => a.CatID == db.Categorys.Select(z => z.ID).FirstOrDefault()).ToList();
            FirstDontar = ListDontarfirst;


            var ListDontar = db.Donators.Where(a => a.CatID == db.Categorys.OrderBy(c => c.ID).Skip(1).Select(z => z.ID).FirstOrDefault()).ToList();
            ScDontar = ListDontar;

            var ListDontarTh = db.Donators.Where(a => a.CatID == db.Categorys.OrderBy(c => c.ID).Skip(2).Select(y => y.ID).FirstOrDefault()).ToList();
            thrDontar = ListDontarTh;

            var homes = new HomePage()
            {
                ListName = List,
                FirstCat = FirstDontar,
                SacandCat = ScDontar,
                ThrCat = thrDontar,
                Cards = db.Ws_Cards.Where(c => c.Main == false).ToList(),
                MainCard = db.Ws_Cards.FirstOrDefault(c => c.Main),
                Video = db.Ws_Video.FirstOrDefault(c => c.Isactive),
                photos = db.Ws_Photos.Where(c => c.Isactive).ToList(),
                WebsiteStatistics = db.WebsiteStatistics.ToList(),
                Donators = db.Donators.ToList(),
                DonatedService = db.DonatServices.ToList(),
                Sliders = db.Ws_Slider.Where(c => c.IsActive).ToList(),
                GoldSponsors = db.Ws_GoldSponsor.ToList(),
                Advertisments = db.Advertisments.Where(c => c.FinishDate > DateTime.Now).ToList(),
                Category = db.Categorys.OrderByDescending(a => a.ID).ToList(),
                DontarTake = db.Donators.OrderByDescending(a => a.ID).Take(15).ToList(),
                TempDiscountService = db.TempDiscountServices.ToList()
            };
            if (Session["visit"] == null)
            {
                var thisvisitor = db.Visitors.FirstOrDefault(c => c.Month == DateTime.Now.Month && c.Month == DateTime.Now.Year);
                if (thisvisitor == null)
                {
                    thisvisitor = new Visitor()
                    {
                        Month = DateTime.Now.Month,
                        Count = 1,
                        Year = DateTime.Now.Year
                    };
                }
                else
                {
                    thisvisitor.Count++;
                }
                db.Visitors.AddOrUpdate(thisvisitor);
                db.SaveChanges();
                Session["visit"] = 1;
            }
            return View(homes);

        }


        //public ActionResult Index()
        //{

        //    ViewBag.Cat = db.Categorys.ToList();
        //    var List = db.Categorys.Take(6).ToList();
        //    List<Donator> FirstDontar = new List<Donator>();
        //    List<Donator> ScDontar = new List<Donator>();
        //    List<Donator> thrDontar = new List<Donator>();

        //    var ListDontarfirst = db.Donators.Where(a => a.CatID == db.Categorys.Select(z => z.ID).FirstOrDefault()).ToList();
        //    FirstDontar = ListDontarfirst;


        //    var ListDontar = db.Donators.Where(a => a.CatID == db.Categorys.OrderBy(c => c.ID).Skip(1).Select(z => z.ID).FirstOrDefault()).ToList();
        //    ScDontar = ListDontar;

        //    var ListDontarTh = db.Donators.Where(a => a.CatID == db.Categorys.OrderBy(c => c.ID).Skip(2).Select(y => y.ID).FirstOrDefault()).ToList();
        //    thrDontar = ListDontarTh;

        //    var homes = new HomePage()
        //    {
        //        ListName = List,
        //        FirstCat = FirstDontar,
        //        SacandCat = ScDontar,
        //        ThrCat = thrDontar,
        //        Cards = db.Ws_Cards.Where(c => c.Main == false).ToList(),
        //        MainCard = db.Ws_Cards.FirstOrDefault(c => c.Main),
        //        Video = db.Ws_Video.FirstOrDefault(c => c.Isactive),
        //        photos = db.Ws_Photos.Where(c => c.Isactive).ToList(),
        //        WebsiteStatistics = db.WebsiteStatistics.ToList(),
        //        Donators = db.Donators.ToList(),
        //        Sliders = db.Ws_Slider.Where(c => c.IsActive).ToList(),
        //        GoldSponsors = db.Ws_GoldSponsor.ToList(),
        //        Advertisments = db.Advertisments.Where(c => c.FinishDate > DateTime.Now).ToList(),
        //        Category = db.Categorys.OrderByDescending(a => a.ID).ToList(),
        //        DontarTake = db.Donators.OrderByDescending(a => a.ID).Take(15).ToList(),


        //    };
        //    if (Session["visit"] == null)
        //    {
        //        var thisvisitor = db.Visitors.FirstOrDefault(c => c.Month == DateTime.Now.Month && c.Month == DateTime.Now.Year);
        //        if (thisvisitor == null)
        //        {
        //            thisvisitor = new Visitor()
        //            {
        //                Month = DateTime.Now.Month,
        //                Count = 1,
        //                Year = DateTime.Now.Year
        //            };
        //        }
        //        else
        //        {
        //            thisvisitor.Count++;
        //        }
        //        db.Visitors.AddOrUpdate(thisvisitor);
        //        db.SaveChanges();
        //        Session["visit"] = 1;
        //    }
        //    return View(homes);
        //}

        public ActionResult GetDontar(int? Id)
        {

            var Dontar = new HomePage()
            {
                DontarById = db.Donators.Where(a => a.ID == Id).FirstOrDefault(),
                DonatorContact = db.DonatorContacts.Where(a => a.DonatorID == Id).ToList(),
                DonatorBranch = db.DonatorBranches.Where(a => a.DonatorID == Id).ToList(),
                DonatedService = db.DonatServices.Where(a => a.DonatorID == Id).ToList(),
            };
            return PartialView(Dontar);
        }


        public ActionResult Branches()
        {
            var Branches = db.DonatorBranches.Take(6).ToList();

            return PartialView(Branches);
        }
        public ActionResult DonatorSearch(int? search)
        {
            ViewBag.Cat = db.Categorys.ToList();
            var donator = db.Donators.FirstOrDefault(c => c.ID == search);
            if (donator != null)
                return RedirectToAction("Details", "Services", new { id = donator.ID });
            return View();
        }

        public ActionResult FeatureService()
        {
            var Service = db.FeatureServices.Take(5).ToList();
            return PartialView(Service);
        }
        public ActionResult DontorService()
        {
            var Service = db.DonatedServices.Take(5).ToList();
            return PartialView(Service);
        }
        public ActionResult Search(string search)
        {
            ViewBag.Cat = db.Categorys.ToList();
            var donator = db.Donators.Where(c => c.OrgName.Contains(search) && (c.IsActive == 1 || c.IsActive == 2)).ToList();
            return View(donator);
        }

        public ActionResult AboutUs()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ContactUs(Ws_Contactus contact)
        {
            ViewBag.Cat = db.Categorys.ToList();
            if (ModelState.IsValid)
            {
                db.Ws_Contactus.Add(contact);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult contactinfo(int id)
        {
            ViewBag.Cat = db.Categorys.ToList();
            ViewBag.id = id;
            var contact = db.WebSiteContactInfoes.FirstOrDefault();
            if (contact == null)
                contact = new WebSiteContactInfo();
            return PartialView(contact);
        }

        public ActionResult Socialmedia()
        {
            ViewBag.Cat = db.Categorys.ToList();
            var so = db.Ws_SocialMedia.Where(c => c.Isactive).ToList();
            return PartialView(so);
        }
        public ActionResult SocialmediaFooter()
        {
            var so = db.Ws_SocialMedia.Where(c => c.Isactive).ToList();
            return PartialView(so);
        }

        [HttpGet]
        public ActionResult DemondCard()
        {
            ViewBag.Cat = db.Categorys.ToList();
            var card = new Cardss()
            {
                Cards = db.Ws_Cards.Where(c => c.Main == false).ToList(),
                MainCard = db.Ws_Cards.FirstOrDefault(c => c.Main)
            };
            return View(card);
        }
        [HttpPost]
        public ActionResult DemondCard(DemondCard card)
        {
            ViewBag.Cat = db.Categorys.ToList();
            if (ModelState.IsValid)
            {
                if (UserManager.FindByEmail(card.Email) == null && UserManager.FindByName(card.Username) == null)
                {
                    var cards = new Card { Name = card.Name, Email = card.Email, Phone = card.Phone };
                    db.Cards.Add(cards);
                    var ind = db.SaveChanges();
                    if (ind > 0)
                    {
                        var usr = new ApplicationUser
                        {
                            UserName = card.Username,
                            Email = card.Email,
                            Type = 4,
                            FullName = card.Name,
                            UserID = cards.ClientID
                        };
                        var suc = UserManager.Create(usr, card.Password);
                        if (!suc.Succeeded)
                        {
                            db.Cards.Remove(cards);
                            db.SaveChanges();
                            ModelState.AddModelError("", "يرجي اخال الكلمة المرورو اكبر من 6 احرف");
                        }
                        else
                        {
                            var user = UserManager.FindByName(usr.UserName);
                            UserManager.AddToRole(user.Id, "Client");
                            SignInManager.PasswordSignIn(user.UserName, card.Password, false, false);
                            return RedirectToAction("Index", "Clients/ClientControlPanel");
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("", "البريد الاليكتروني او اسم المستخدم متواجد بالفعل يرجي تغييرهم");
                }


            }

            return View(card);
        }

        public ActionResult Login()
        {
            ViewBag.Cat = db.Categorys.ToList();
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            ViewBag.Cat = db.Categorys.ToList();
            var user = UserManager.FindByName(model.Username);
            if (user != null)
            {
                var result = SignInManager.PasswordSignIn(model.Username, model.Password, model.RememberMe, shouldLockout: false);
                switch (result)
                {
                    case SignInStatus.Success:
                        {
                            await SignInAsync(user, model.RememberMe);

                            if (UserManager.IsInRole(user.Id, "Admin"))
                            {
                                return RedirectToAction("Index", "Admin/Admin");
                            }
                            if (UserManager.IsInRole(user.Id, "Employee"))
                            {
                                return RedirectToAction("Index", "Employee/Empcpanel");
                            }
                            if (UserManager.IsInRole(user.Id, "Donator"))
                            {
                                return RedirectToAction("Index", "Donators/cpanel");
                            }
                            if (UserManager.IsInRole(user.Id, "Client"))
                            {
                                return RedirectToAction("Index", "Clients/ClientControlPanel");
                            }

                            return RedirectToAction("Login", "Home");
                        }
                    default:
                        return View(model);
                }
            }

            return View(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            ViewBag.Cat = db.Categorys.ToList();
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }


        #region Helpers

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public ActionResult ContactUs()
        {
            ViewBag.Cat = db.Categorys.ToList();
            return View();
        }

        public ActionResult AlbomPhoto()
        {
            ViewBag.Cat = db.Categorys.ToList();
            var Albom = db.Ws_Photos.ToList();
            return View(Albom);
        }
        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }


        #endregion


    }
}