﻿using SaveIt.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SaveIt.Controllers
{
    public class ServicesController : Controller
    {
        SaveItEntities db = new SaveItEntities();
        public ActionResult Index()
        {
            ViewBag.Cat = db.Categorys.ToList();
            ViewBag.name = "الاقسام";
            var cates = db.Categorys.ToList();
            return View(cates);

        }

        public ActionResult Categorys(int? id, string type)
        {
            ViewBag.Cat = db.Categorys.ToList();
            ViewBag.name = "الاقسام";
            var cates = db.Categorys.ToList();
            if (id != null)
            {
                ViewBag.Donts = id;
            }
            else
            {
                ViewBag.Donts = 2;
            }
            return View(cates);
        }

        public ActionResult _Category()
        {
            ViewBag.Cat = db.Categorys.ToList();
            var cates = db.Categorys.ToList();
            return PartialView(cates);
        }
        public ActionResult RegionLayout()
        {
            ViewBag.Cat = db.Categorys.ToList();
            var Regions = db.Regions.ToList();
            return PartialView(Regions);
        }
        public ActionResult _Regions()
        {
            ViewBag.Cat = db.Categorys.ToList();
            var Regions = db.Regions.ToList();
            return PartialView(Regions);
        }
        public ActionResult Regions()
        {
            ViewBag.Cat = db.Categorys.ToList();
            var Regions = db.Regions.ToList();
            return PartialView(Regions);
        }

        public ActionResult Donators(int? id, string type)
        {
            ViewBag.Cat = db.Categorys.ToList();
            var donators = new List<Donator>();
            if (id == null)
            {
                //return RedirectToAction("Index");
                donators = db.Donators.Where(c => c.IsActive == 1 || c.IsActive == 2).ToList();
                return View(donators);
            }
            ViewBag.name = "الخدمات الدائمة والمؤقتة";
            if (type == "r")
                donators = db.Donators.Where(c => c.RegionId == id).Where(c => c.IsActive == 1 || c.IsActive == 2).ToList();
            else
                donators = db.Donators.Where(c => c.CatID == id).Where(c => c.IsActive == 1 || c.IsActive == 2).ToList();

            return View(donators);
        }

        public PartialViewResult Donts(int? id, string type)
        {
            ViewBag.DontName = db.Categorys.Where(x => x.ID == id).Select(x => x.Name).FirstOrDefault();

            ViewBag.Cat = db.Categorys.ToList();
            var donators = new List<Donator>();
            if (id == null)
            {
                donators = db.Donators.Where(c => c.IsActive == 1 || c.IsActive == 2).ToList();
                return PartialView(donators);
            }
            ViewBag.name = "الخدمات الدائمة والمؤقتة";
            if (type == "r")
                donators = db.Donators.Where(c => c.RegionId == id).Where(c => c.IsActive == 1 || c.IsActive == 2).ToList();
            else
                donators = db.Donators.Where(c => c.CatID == id).Where(c => c.IsActive == 1 || c.IsActive == 2).ToList();

            return PartialView(donators);
        }

        public ActionResult Details(int? id)
        {
            ViewBag.Discount = db.DonatServices.Where(x => x.DonatorID == id).Select(x => x.Discount).FirstOrDefault();
            ViewBag.Cat = db.Categorys.ToList();
            if (id != null)
            {
                var don = db.Donators.Find(id);
                if (don != null)
                {
                    ViewBag.Branches = db.DonatorBranches.Where(x => x.DonatorID == id).ToList();
                    return View(don);
                }
            }

            return RedirectToAction("Index");
        }
    }
    public class GetSevices
    {

        public List<Category> Categorys { get; set; }

        public List<Region> Regions { get; set; }
    }
}