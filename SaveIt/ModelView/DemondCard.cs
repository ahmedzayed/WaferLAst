﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SaveIt.ModelView
{
    public class DemondCard
    {
        [Required(ErrorMessage ="ادخل البريد الاليكترونى")]
        [Display(Name = "البريد الاليكتروني")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "ادخل البريد الاليكتروني الصحيح")]
        public string Email { get; set; }
        [Required(ErrorMessage ="ادخل الاسم بالكامل")]
        [Display(Name = "الاسم بالكامل")]
        public string Name { get; set; }
        [Required(ErrorMessage = "ادخل رقم الجوال ")]
        [Display(Name = "رقم الجوال")]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }
        [Required(ErrorMessage = "ادخل اسم المستخدم")]
        [Display(Name = "اسم المستخدم")]
        public string Username { get; set; }
        [Required(ErrorMessage = "ادخل كلمه المرور")]
        [StringLength(100, ErrorMessage = "يجب ان يكون كلمة المرور  اكبر من 6 احرف", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "كلمة المرور")]
        public string Password { get; set; }
        [Required(ErrorMessage = "ادخل كلمه المرور")]
        [DataType(DataType.Password)]
        [Display(Name = "تاكيد كلمة المرور")]
        [Compare("Password", ErrorMessage = "تاكيد كلمة المرور غير مطابق")]
        public string ConfirmPassword { get; set; }
       
    }
}