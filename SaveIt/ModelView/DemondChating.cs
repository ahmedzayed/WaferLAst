﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaveIt.ModelView
{
    public class DemondChating
    {
        public int ID { get; set; }
        public int? UserID { get; set; }
        public string UserName { get; set; }
        public string Type { get; set; }
        public DateTime? Create { get; set; }
    }
}