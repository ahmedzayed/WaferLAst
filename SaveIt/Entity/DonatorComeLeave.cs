//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SaveIt.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class DonatorComeLeave
    {
        public int id { get; set; }
        public Nullable<int> donatorID { get; set; }
        public string ComeOn { get; set; }
        public string LeaveOn { get; set; }
    }
}
