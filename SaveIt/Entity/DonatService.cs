//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SaveIt.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class DonatService
    {
        public DonatService()
        {
            this.ClientRegisterations = new HashSet<ClientRegisteration>();
            this.TempDiscountServices = new HashSet<TempDiscountService>();
        }
    
        public int ID { get; set; }
        public Nullable<int> DonatorID { get; set; }
        public string Service { get; set; }
        public Nullable<int> Discount { get; set; }
        public Nullable<bool> IsGeneral { get; set; }
    
        public virtual ICollection<ClientRegisteration> ClientRegisterations { get; set; }
        public virtual Donator Donator { get; set; }
        public virtual ICollection<TempDiscountService> TempDiscountServices { get; set; }
    }
}
