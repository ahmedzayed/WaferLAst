﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using SaveIt.Entity;
using SaveIt.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using SaveIt.Areas.Admin.Controllers;
using SaveIt.ModelView;
using System.Data.Entity.Migrations;
using System.Data.Entity.Infrastructure;
using Microsoft.AspNet.Identity.EntityFramework;
using SaveIt.Infrastructure;

namespace SaveIt.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        SaveItEntities db = new SaveItEntities();
        public HomeController()
        {

        }

        public HomeController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Home()
        {
            ViewBag.DontsNo = db.WebsiteStatistics.Where(x => x.Title == "المانحين").Select(x => x.Count).FirstOrDefault();
            ViewBag.Subs = db.WebsiteStatistics.Where(x => x.Title == "المشتركين").Select(x => x.Count).FirstOrDefault();
            ViewBag.Disc = db.WebsiteStatistics.Where(x => x.Title == "الخصومات تصل الي").Select(x => x.Count).FirstOrDefault();

            ViewBag.Cat = db.Categorys.ToList();
            var List = db.Categorys.Take(6).ToList();
            List<Donator> FirstDontar = new List<Donator>();
            List<Donator> ScDontar = new List<Donator>();
            List<Donator> thrDontar = new List<Donator>();

            var ListDontarfirst = db.Donators.Where(a => a.CatID == db.Categorys.Select(z => z.ID).FirstOrDefault()).ToList();
            FirstDontar = ListDontarfirst;


            var ListDontar = db.Donators.Where(a => a.CatID == db.Categorys.OrderBy(c => c.ID).Skip(1).Select(z => z.ID).FirstOrDefault()).ToList();
            ScDontar = ListDontar;

            var ListDontarTh = db.Donators.Where(a => a.CatID == db.Categorys.OrderBy(c => c.ID).Skip(2).Select(y => y.ID).FirstOrDefault()).ToList();
            thrDontar = ListDontarTh;

            var homes = new HomePage()
            {
                ListName = List,
                FirstCat = FirstDontar,
                SacandCat = ScDontar,
                ThrCat = thrDontar,
                Cards = db.Ws_Cards.Where(c => c.Main == false).ToList(),
                MainCard = db.Ws_Cards.FirstOrDefault(c => c.Main),
                Video = db.Ws_Video.FirstOrDefault(c => c.Isactive),
                photos = db.Ws_Photos.Where(c => c.Isactive).ToList(),
                WebsiteStatistics = db.WebsiteStatistics.ToList(),
                Donators = db.Donators.ToList(),
                DonatedService = db.DonatServices.ToList(),
                Sliders = db.Ws_Slider.Where(c => c.IsActive).ToList(),
                GoldSponsors = db.Ws_GoldSponsor.ToList(),
                Advertisments = db.Advertisments.Where(c => c.FinishDate > DateTime.Now).ToList(),
                Category = db.Categorys.OrderByDescending(a => a.ID).ToList(),
                DontarTake = db.Donators.OrderByDescending(a => a.ID).Take(15).ToList(),
                TempDiscountService = db.TempDiscountServices.ToList(),
                Event = db.Events.ToList(),
                News = db.News.ToList()
            };
            if (Session["visit"] == null)
            {
                var thisvisitor = db.Visitors.FirstOrDefault(c => c.Month == DateTime.Now.Month && c.Month == DateTime.Now.Year);
                if (thisvisitor == null)
                {
                    thisvisitor = new Visitor()
                    {
                        Month = DateTime.Now.Month,
                        Count = 1,
                        Year = DateTime.Now.Year
                    };
                }
                else
                {
                    thisvisitor.Count++;
                }
                db.Visitors.AddOrUpdate(thisvisitor);
                db.SaveChanges();
                Session["visit"] = 1;
            }
            return View(homes);

        }


        public ActionResult Index()
        {

            ViewBag.Cat = db.Categorys.ToList();
            var List = db.Categorys.Take(6).ToList();
            List<Donator> FirstDontar = new List<Donator>();
            List<Donator> ScDontar = new List<Donator>();
            List<Donator> thrDontar = new List<Donator>();

            var ListDontarfirst = db.Donators.Where(a => a.CatID == db.Categorys.Select(z => z.ID).FirstOrDefault()).ToList();
            FirstDontar = ListDontarfirst;


            var ListDontar = db.Donators.Where(a => a.CatID == db.Categorys.OrderBy(c => c.ID).Skip(1).Select(z => z.ID).FirstOrDefault()).ToList();
            ScDontar = ListDontar;

            var ListDontarTh = db.Donators.Where(a => a.CatID == db.Categorys.OrderBy(c => c.ID).Skip(2).Select(y => y.ID).FirstOrDefault()).ToList();
            thrDontar = ListDontarTh;

            var homes = new HomePage()
            {
                ListName = List,
                FirstCat = FirstDontar,
                SacandCat = ScDontar,
                ThrCat = thrDontar,
                Cards = db.Ws_Cards.Where(c => c.Main == false).ToList(),
                MainCard = db.Ws_Cards.FirstOrDefault(c => c.Main),
                Video = db.Ws_Video.FirstOrDefault(c => c.Isactive),
                photos = db.Ws_Photos.Where(c => c.Isactive).ToList(),
                WebsiteStatistics = db.WebsiteStatistics.ToList(),
                Donators = db.Donators.ToList(),
                Sliders = db.Ws_Slider.Where(c => c.IsActive).ToList(),
                GoldSponsors = db.Ws_GoldSponsor.ToList(),
                Advertisments = db.Advertisments.Where(c => c.FinishDate > DateTime.Now).ToList(),
                Category = db.Categorys.OrderByDescending(a => a.ID).ToList(),
                DontarTake = db.Donators.OrderByDescending(a => a.ID).Take(15).ToList(),


            };
            if (Session["visit"] == null)
            {
                var thisvisitor = db.Visitors.FirstOrDefault(c => c.Month == DateTime.Now.Month && c.Month == DateTime.Now.Year);
                if (thisvisitor == null)
                {
                    thisvisitor = new Visitor()
                    {
                        Month = DateTime.Now.Month,
                        Count = 1,
                        Year = DateTime.Now.Year
                    };
                }
                else
                {
                    thisvisitor.Count++;
                }
                db.Visitors.AddOrUpdate(thisvisitor);
                db.SaveChanges();
                Session["visit"] = 1;
            }
            return View(homes);
        }

        public ActionResult GetDontar(int? Id)
        {

            var Dontar = new HomePage()
            {
                DontarById = db.Donators.Where(a => a.ID == Id).FirstOrDefault(),
                DonatorContact = db.DonatorContacts.Where(a => a.DonatorID == Id).ToList(),
                DonatorBranch = db.DonatorBranches.Where(a => a.DonatorID == Id).ToList(),
                DonatedService = db.DonatServices.Where(a => a.DonatorID == Id).ToList(),
            };
            return PartialView(Dontar);
        }

        public ActionResult Branches()
        {
            var Branches = db.DonatorBranches.Take(6).ToList();

            return PartialView(Branches);
        }

        public ActionResult DonatorSearch(int? search)
        {
            ViewBag.Cat = db.Categorys.ToList();
            var donator = db.Donators.FirstOrDefault(c => c.ID == search);
            if (donator != null)
                return RedirectToAction("Details", "Services", new { id = donator.ID });
            return View();
        }

        public ActionResult FeatureService()
        {
            var Service = db.FeatureServices.Take(5).ToList();
            return PartialView(Service);
        }

        public ActionResult DontorService()
        {
            var Service = db.DonatedServices.Take(5).ToList();
            return PartialView(Service);
        }

        public ActionResult Search(string search)
        {
            ViewBag.Cat = db.Categorys.ToList();
            var donator = db.Donators.Where(c => c.OrgName.Contains(search) && (c.IsActive == 1 || c.IsActive == 2)).ToList();
            return View(donator);
        }

        public ActionResult AboutUs()
        {
            var evc = db.EvaluationClients.ToList();
            ViewBag.DontsNo = db.WebsiteStatistics.Where(x => x.Title == "المانحين").Select(x => x.Count).FirstOrDefault();
            ViewBag.Subs = db.WebsiteStatistics.Where(x => x.Title == "المشتركين").Select(x => x.Count).FirstOrDefault();
            ViewBag.Disc = db.WebsiteStatistics.Where(x => x.Title == "الخصومات تصل الي").Select(x => x.Count).FirstOrDefault();

            return View(evc);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ContactUs(Ws_Contactus contact)
        {
            ViewBag.Cat = db.Categorys.ToList();
            if (ModelState.IsValid)
            {
                Ws_Contactus con = new Ws_Contactus() { Name = contact.Name, Email = contact.Email, Phone = contact.Phone, Messages = contact.Messages };
                db.Ws_Contactus.Add(con);
                db.SaveChanges();
            }
            return RedirectToAction("Home");
        }

        public ActionResult contactinfo(int id)
        {
            ViewBag.Cat = db.Categorys.ToList();
            ViewBag.id = id;
            var contact = db.WebSiteContactInfoes.FirstOrDefault();
            if (contact == null)
                contact = new WebSiteContactInfo();
            return PartialView(contact);
        }

        public ActionResult Socialmedia()
        {
            ViewBag.Cat = db.Categorys.ToList();
            var so = db.Ws_SocialMedia.Where(c => c.Isactive).ToList();
            return PartialView(so);
        }

        public ActionResult SocialmediaFooter()
        {
            var so = db.Ws_SocialMedia.Where(c => c.Isactive).ToList();
            return PartialView(so);
        }

        [HttpGet]
        public ActionResult DemondCard()
        {
            ViewBag.Cat = db.Categorys.ToList();
            var card = new Cardss()
            {
                Cards = db.Ws_Cards.Where(c => c.Main == false).ToList(),
                MainCard = db.Ws_Cards.FirstOrDefault(c => c.Main)
            };
            return View(card);
        }

        [HttpPost]
        public ActionResult DemondCard(DemondCard card)
        {
            ViewBag.Cat = db.Categorys.ToList();
            if (ModelState.IsValid)
            {
                if (UserManager.FindByEmail(card.Email) == null && UserManager.FindByName(card.Username) == null)
                {
                    var cards = new Card { Name = card.Name, Email = card.Email, Phone = card.Phone };
                    db.Cards.Add(cards);
                    var ind = db.SaveChanges();
                    if (ind > 0)
                    {
                        var usr = new ApplicationUser
                        {
                            UserName = card.Username,
                            Email = card.Email,
                            Type = 4,
                            FullName = card.Name,
                            UserID = cards.ClientID
                        };
                        var suc = UserManager.Create(usr, card.Password);
                        if (!suc.Succeeded)
                        {
                            db.Cards.Remove(cards);
                            db.SaveChanges();
                            ViewBag.Error = "يرجي اخال الكلمة المرورو اكبر من 6 احرف";
                        }
                        else
                        {
                            //Notification.SendPushNotification(card.Name);
                            var user = UserManager.FindByName(usr.UserName);
                            UserManager.AddToRole(user.Id, "Client");
                            SignInManager.PasswordSignIn(user.UserName, card.Password, false, false);
                            return RedirectToAction("Home");
                        }
                    }
                }
                else
                {
                    ViewBag.Error = "البريد الاليكتروني او اسم المستخدم متواجد بالفعل يرجي تغييرهم";
                }


            }

            return View();
        }

        public ActionResult Gallary()
        {
            var x = db.Ws_Photos.ToList();
            return View(x);
        }

        [EncryptedActionParameter]
        public ActionResult Events(int id)
        {
            var ev = db.Events.Where(x => x.Id == id).FirstOrDefault();

            return View(ev);
        }

        public ActionResult ShowEvents()
        {
            var x = db.Events.ToList();
            return View(x);
        }

        [EncryptedActionParameter]
        public ActionResult News(int id)
        {
            var ev = db.News.Where(x => x.Id == id).FirstOrDefault();

            return View(ev);
        }

        public ActionResult ShowNews()
        {
            var x = db.News.ToList();
            return View(x);
        }
        [EncryptedActionParameter]
        public ActionResult Products(int? id)
        {
            //Session["UserID"] = "60629e05-5a82-47f1-a745-1c096b7bbd47";
            Donator dn = db.Donators.Where(m => m.ID == id).FirstOrDefault();
            ViewBag.DonatorName = dn.OrgName;
            ViewBag.DonatorImage = dn.Photo;
            List<ProductsCategory> cat = db.ProductsCategories.Where(m => m.DonatorID == id).ToList();
            return View(cat);
        }



        public JsonResult AddToCart(int pid)
        {
            int m = 0;
            var usr = User.Identity.GetUserId();

            var UsrCrt = db.Carts.Where(a => a.UserID == usr);
            AddCart(pid);
            if (db.Carts.Where(a => a.UserID == usr && string.IsNullOrEmpty(a.Role)).Count() > 0)
            {

                var pd = db.CartDetails.Where(a => a.ProductID == pid && a.CartID == UsrCrt.Max(s => s.ID)).FirstOrDefault();

                if (pd != null)
                {
                    pd.Quantity = int.Parse(pd.Quantity.ToString()) + 1;
                    // pd.Price = pd.Quantity * (pd.Price);
                    var Product = db.Products.Where(a => a.ID == pid).FirstOrDefault();
                    pd.Price = pd.Quantity * (Product.Price - ((Product.Price * Product.Discount) / 100));
                }
                else
                {
                    AddCart(pid);
                    CartDetail crtDetail = new CartDetail();
                    crtDetail.ProductID = pid;
                    crtDetail.Quantity = 1;
                    crtDetail.CartID = db.Carts.Where(a => a.UserID == usr).Max(a => a.ID);

                    var prc = db.Products.Where(x => x.ID == pid).FirstOrDefault().Price;
                    var dis = db.Products.Where(x => x.ID == pid).FirstOrDefault().Discount;
                    var ttl = (prc - (prc * dis) / 100);
                    crtDetail.Price = ttl;
                    db.CartDetails.Add(crtDetail);

                }
            }
            else
            {
                AddCart(pid);
                CartDetail crtDetail = new CartDetail();
                crtDetail.ProductID = pid;
                crtDetail.Quantity = 1;
                crtDetail.CartID = db.Carts.Where(a => a.UserID == usr).Max(a => a.ID);

                var prc = db.Products.Where(x => x.ID == pid).FirstOrDefault().Price;
                var dis = db.Products.Where(x => x.ID == pid).FirstOrDefault().Discount;
                var ttl = (prc - (prc * dis) / 100);
                crtDetail.Price = ttl;
                db.CartDetails.Add(crtDetail);

            }

            m = db.SaveChanges();

            return new JsonResult { Data = pid, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public void AddCart(int prID)
        {
            var userID = User.Identity.GetUserId();
            var cat = db.Carts.Where(a => a.UserID == userID && string.IsNullOrEmpty(a.Role));
            if (cat.Count() == 0)
            {
                Cart ct = new Cart();
                ct.CreateDate = DateTime.Now;

                var Product = db.Products.Where(a => a.ID == prID).FirstOrDefault();

                ct.Total = Product.Price - ((Product.Price * Product.Discount) / 100);

                ct.UserID = userID;
                db.Carts.Add(ct);
                db.SaveChanges();
            }

        }

        public JsonResult UpdateProducts(int Pid, int count)
        {

            var usr = User.Identity.GetUserId();
            var p = db.CartDetails.Where(a => a.ProductID == Pid && a.Cart.UserID == usr).FirstOrDefault();
            Product products = db.Products.Where(a => a.ID == Pid).FirstOrDefault();
            p.Quantity = count;
            p.Price = count * (products.Price - (products.Price * products.Discount) / 100);
            int m = 0;
            m = db.SaveChanges();
            UpdateCart(int.Parse(p.CartID.ToString()), count, Pid);
            return new JsonResult { Data = m, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult DeleteCart(int Pid, int Cid)
        {
            int m = 0;
            var usr = User.Identity.GetUserId();
            var p = db.CartDetails.Where(a => a.ProductID == Pid && a.Cart.UserID == usr && a.CartID == Cid).FirstOrDefault();

            db.CartDetails.Remove(p);
            m = db.SaveChanges();

            return new JsonResult { Data = m, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public void UpdateCart(int catID, int count, int prID)
        {

            var ct = db.Carts.Where(a => a.ID == catID).FirstOrDefault();
            var Product = db.CartDetails.Where(a => a.CartID == ct.ID);
            double sum = 0;
            foreach (var item in Product)
            {
                sum = sum + double.Parse(item.Price.ToString());
            }
            ct.Total = sum;
            db.SaveChanges();

        }

        public JsonResult Refresh(int tdID, int count)
        {
            //int m = 0;
            var usr = User.Identity.GetUserId();
            var ct = db.Carts.Where(a => a.UserID == usr && a.ID == tdID).FirstOrDefault();

            var prc = db.CartDetails.Where(a => a.CartID == tdID).FirstOrDefault();

            var m = prc.Price * count;
            prc.Quantity = count;
            prc.Price = m;
            db.SaveChanges();
            return new JsonResult { Data = m, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }


        public ActionResult _Products(int Catid)
        {
            List<Product> prd = db.Products.Where(m => m.ProductCatID == Catid).ToList();
            return PartialView(prd);
        }

        public ActionResult Login()
        {
            ViewBag.Cat = db.Categorys.ToList();
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            Session["name"] = model.Username;
            ViewBag.Cat = db.Categorys.ToList();
            var user = UserManager.FindByName(model.Username);
            if (user != null)
            {
                var result = SignInManager.PasswordSignIn(model.Username, model.Password, model.RememberMe, shouldLockout: false);

                switch (result)
                {
                    case SignInStatus.Success:
                        {
                            await SignInAsync(user, model.RememberMe);

                            if (UserManager.IsInRole(user.Id, "Admin"))
                            {
                                Session["UserID"] = user.Id;
                                return RedirectToAction("Index", "Admin/Admin");
                            }
                            else if (UserManager.IsInRole(user.Id, "Donator"))
                            {
                                Session["UserID"] = user.Id;
                                var dn = db.Donators.Where(x => x.Username == user.UserName).FirstOrDefault().ID;
                                Session["DonatorID"] = dn;
                                return RedirectToAction("Index", "Donators/PrdCategory");

                            }
                            else
                            {
                                Session["UserID"] = user.Id;
                                return RedirectToAction("Home", "Home");
                            }
                            //if (UserManager.IsInRole(user.Id, "Employee"))
                            //{
                            //    return RedirectToAction("Index", "Employee/Empcpanel");
                            //}

                            //if (UserManager.IsInRole(user.Id, "Client"))
                            //{
                            //    return RedirectToAction("Index", "Clients/ClientControlPanel");
                            //}

                            //return RedirectToAction("Home");
                        }
                    default:
                        return View(model);
                }
            }
            else
            {
                ViewBag.Error = "تأكد من الايميل و الباسورد";
            }

            return View(model);
        }

        [Authorize]
        [HttpGet]
        public ActionResult ResetPassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> ResetPassword(string oldPassword, string newPassword, string ComparePassword)
        {
            if (newPassword == ComparePassword)
            {
                var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

                var usrID = User.Identity.GetUserId();
                var usr = db.AspNetUsers.Where(x => x.Id == usrID).FirstOrDefault();
                //var user = UserManager.FindByName(usr.UserName);
                var result = await userManager.RemovePasswordAsync(usrID);
                if (result.Succeeded)
                {

                    result = await userManager.AddPasswordAsync(usrID, newPassword);
                    if (result.Succeeded)
                    {
                        ViewBag.Success = "تم تحديث كلمه مرور الجديده ";
                    }
                    else
                    {
                        ViewBag.ErrorCompare = "كلمه مرور الجديده خطأ";
                        //return RedirectToAction("Details", "Home");

                    }
                }
                else
                {
                    ViewBag.Error = "كلمه مرور القديمه خطأ";
                    //return RedirectToAction("Donts", "Home");

                }
                return View();
            }
            else
            {
                ViewBag.ErrorCompare = "كلمه مرور الجديده غير متشابهه";
                return View();
            }
        }

        //[HttpPost]
        //public ActionResult ResetPassword(string oldPassword, string newPassword, string ComparePassword)
        //{
        //    var usrID = User.Identity.GetUserId();
        //    var usr = db.AspNetUsers.Where(x => x.Id == usrID).FirstOrDefault();
        //    var ps = UserManager.PasswordHasher.HashPassword(oldPassword);


        //    if (usr.PasswordHash == ps)
        //    {
        //        if (newPassword == ComparePassword)
        //        {
        //            //ApplicationDbContext dbContext = new ApplicationDbContext();
        //            //UserStore<ApplicationUser> userStore = new UserStore<ApplicationUser>(dbContext);
        //            //UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(userStore);
        //            //string userID = User.Identity.GetUserId();
        //            //string hashpass = userManager.PasswordHasher.HashPassword(newPassword);
        //            //ApplicationUser cuser =await userStore.FindByIdAsync(userID);
        //            //userStore.SetPasswordHashAsync(usr,newPassword);
        //            //UserManager.ResetPassword(usr.Id, usr.SecurityStamp, newPassword);

        //            var tok = UserManager.GeneratePasswordResetToken(usr.Id);
        //            UserManager.ResetPassword(usr.Id, tok, newPassword);

        //            db.SaveChanges();
        //            ViewBag.Success = "تم تحديث كلمه مرور الجديده ";
        //        }
        //        else
        //        {
        //            ViewBag.ErrorCompare = "كلمه مرور الجديده غير متشابهه";
        //        }
        //    }
        //    else
        //    {
        //        ViewBag.Error = "كلمه مرور القديمه خطأ";
        //    }

        //    return View();
        //}

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            ViewBag.Cat = db.Categorys.ToList();
            AuthenticationManager.SignOut();
            return RedirectToAction("Home", "Home");
        }


        #region Helpers

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public ActionResult ContactUs()
        {
            ViewBag.Cat = db.Categorys.ToList();
            return View();
        }

        public ActionResult AlbomPhoto()
        {
            ViewBag.Cat = db.Categorys.ToList();
            var Albom = db.Ws_Photos.ToList();
            return View(Albom);
        }
        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }


        #endregion


    }
}