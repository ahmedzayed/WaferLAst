﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using SaveIt.Entity;
using SaveIt.Infrastructure;
using SaveIt.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SaveIt.Controllers
{
    public class ServicesController : Controller
    {
        private ApplicationUserManager _userManager;
        private ApplicationSignInManager _signInManager;

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        SaveItEntities db = new SaveItEntities();


        public ActionResult Index()
        {
            ViewBag.Cat = db.Categorys.ToList();
            ViewBag.name = "الاقسام";
            var cates = db.Categorys.ToList();
            return View(cates);

        }

        public ActionResult Categorys(int? id, string type)
        {
            ViewBag.Cat = db.Categorys.ToList();
            ViewBag.name = "الاقسام";
            var cates = db.Categorys.ToList();
            if (id != null)
            {
                ViewBag.Donts = id;
            }
            else
            {
                ViewBag.Donts = 2;
            }
            return View(cates);
        }
        public ActionResult PrdCategorys(int? id, string type)
        {
            ViewBag.Cat = db.Categorys.ToList();
            ViewBag.name = "الاقسام";
            var cates = db.Categorys.ToList();

            if (id != null)
            {
                ViewBag.Donts = id;
            }
            else
            {
                ViewBag.Donts = 2;
            }
            return View(cates);
        }

        public ActionResult _Category()
        {
            ViewBag.Cat = db.Categorys.ToList();
            var cates = db.Categorys.ToList();
            return PartialView(cates);
        }

        public PartialViewResult _PrdCategory()
        {
            ViewBag.Cat = db.Categorys.ToList();

            //ViewBag.Count = db.Products.Select(x => x.DontorID).Count();

            var objDonators = db.ProductsCategories.Select(x => x.DonatorID).ToList();

            ViewBag.Count = objDonators.Count();

            var cates = db.Categorys.ToList();

            return PartialView(cates);
        }

        public ActionResult RegionLayout()
        {
            ViewBag.Cat = db.Categorys.ToList();
            var Regions = db.Regions.ToList();
            return PartialView(Regions);
        }

        public PartialViewResult _Regions()
        {
            ViewBag.Cat = db.Categorys.ToList();
            var Regions = db.Regions.ToList();
            return PartialView(Regions);
        }

        public PartialViewResult _PrdRegions()
        {
            ViewBag.Cat = db.Categorys.ToList();
            var Regions = db.Regions.ToList();
            return PartialView(Regions);
        }

        public ActionResult Regions()
        {
            ViewBag.Cat = db.Categorys.ToList();
            var Regions = db.Regions.ToList();
            return PartialView(Regions);
        }

        public PartialViewResult PrdDonts(int? id, string type)
        {
            if (type == "d")
                ViewBag.DontName = db.Categorys.Where(x => x.ID == id).Select(x => x.Name).FirstOrDefault();
            else
                ViewBag.RgnsName = db.Regions.Where(x => x.ID == id).Select(x => x.Name).FirstOrDefault();

            ViewBag.Cat = db.Categorys.ToList();
            var donators = new List<Donator>();
            if (id == null)
            {
                donators = db.Donators.Where(c => c.IsActive == 1 || c.IsActive == 2).ToList();
                return PartialView(donators);
            }
            ViewBag.name = "الخدمات الدائمة والمؤقتة";

            if (type == "r")
            {
                var objDonators = db.ProductsCategories.Select(x => x.DonatorID).ToList();
                donators = db.Donators.Where(x => objDonators.Contains(x.ID) && x.RegionId == id).ToList();

                //donators = db.Donators.Where(c => c.RegionId == id).Where(c => c.IsActive == 1 || c.IsActive == 2).ToList();
            }
            else
            {
                var objDonators = db.ProductsCategories.Select(x => x.DonatorID).ToList();
                donators = db.Donators.Where(x => objDonators.Contains(x.ID) && x.CatID == id).ToList();

            }
            return PartialView(donators);
        }

        public PartialViewResult Donts(int? id, string type)
        {
            if (type == "d")
                ViewBag.DontName = db.Categorys.Where(x => x.ID == id).Select(x => x.Name).FirstOrDefault();
            else
                ViewBag.RgnsName = db.Regions.Where(x => x.ID == id).Select(x => x.Name).FirstOrDefault();

            ViewBag.Cat = db.Categorys.ToList();
            var donators = new List<Donator>();
            if (id == null)
            {
                donators = db.Donators.Where(c => c.IsActive == 1 || c.IsActive == 2).ToList();
                return PartialView(donators);
            }
            ViewBag.name = "الخدمات الدائمة والمؤقتة";
            if (type == "r")
            {
                donators = db.Donators.Where(c => c.RegionId == id).Where(c => c.IsActive == 1 || c.IsActive == 2).ToList();
            }
            else if (type == "d")
            {
                donators = db.Donators.Where(c => c.CatID == id).Where(c => c.IsActive == 1 || c.IsActive == 2).ToList();
            }
            else if (type == "p")
            {
                var DID = db.Products.Select(a => a.DontorID).ToList();
                foreach (var item in DID)
                {
                    donators.AddRange(db.Donators.Where(c => c.ID == item));
                }
            }
            return PartialView(donators);
        }
        [EncryptedActionParameter]

        public ActionResult Details(int? id)
        {
            ViewBag.Discount = db.DonatServices.Where(x => x.DonatorID == id).Select(x => x.Discount).FirstOrDefault();
            ViewBag.Cat = db.Categorys.ToList();
            if (id != null)
            {
                var don = db.Donators.Find(id);
                if (don != null)
                {
                    ViewBag.Branches = db.DonatorBranches.Where(x => x.DonatorID == id).ToList();
                    return View(don);
                }
            }

            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult AddDonator()
        {
            ViewData["CatName"] = new SelectList(db.Categorys.ToList(), "ID", "Name");
            ViewData["RegionName"] = new SelectList(db.Regions.ToList(), "ID", "Name");

            return View();
        }

        [HttpPost]
        public ActionResult AddDonator(Donator dnt)
        {
            db.Donators.Add(dnt);
            db.SaveChanges();
            return View();
        }

        [HttpGet]
        public ActionResult Cart()
        {
            string user = User.Identity.GetUserId();
            int maxID = db.Carts.Where(a => a.UserID == user).Max(a => a.ID);
            return View(db.Carts.Where(a => a.ID == maxID).FirstOrDefault());
        }

        [HttpGet]
        public PartialViewResult _Cart1()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult _Cart1(CartUser usr, string confPass, int? Id)
        {
            var x = Request.QueryString["type"];

            if (usr.Password == confPass)
            {
                if (UserManager.FindByEmail(usr.Email) == null && UserManager.FindByName(usr.UserName) == null)
                {
                    var carts = new CartUser
                    { UserName = usr.UserName, Email = usr.Email, Phone = usr.Phone, FullName = usr.FullName };
                    db.CartUsers.Add(carts);
                    var ind = db.SaveChanges();
                    if (ind > 0)
                    {
                        var user = new ApplicationUser
                        {
                            UserName = usr.UserName,
                            Email = usr.Email,
                            Type = 4,
                            FullName = usr.FullName
                        };
                        var suc = UserManager.Create(user, usr.Password);
                        if (!suc.Succeeded)
                        {
                            db.CartUsers.Remove(carts);
                            db.SaveChanges();
                            Session["Error"] = "يرجي اخال الكلمة المرورو اكبر من 6 احرف";
                        }
                        else
                        {
                            var users = UserManager.FindByName(usr.UserName);
                            UserManager.AddToRole(users.Id, "Client");
                            SignInManager.PasswordSignIn(usr.UserName, usr.Password, false, false);
                            return RedirectToAction("Products/" + Id, "Home", new { type = x });

                        }
                    }
                }
                else
                {
                    Session["Error"] = "البريد الاليكتروني او اسم المستخدم متواجد بالفعل يرجي تغييرهم";
                }
            }
            else
            {
                Session["Error"] = "كلمة مرور غير متشايهه";
                return View("Cart");
            }
            return View("Cart");

        }

        [HttpGet]
        public ActionResult _Cart2()
        {
            if (User.Identity.IsAuthenticated)
            {
                return View("");
            }
            return PartialView();
        }

        [HttpPost]
        public ActionResult _Cart2(LoginViewModel model, int? Id)
        {
            var x = Request.QueryString["type"];
            var user = UserManager.FindByName(model.Username);
            if (user != null)
            {
                var result = SignInManager.PasswordSignIn(model.Username, model.Password, model.RememberMe, shouldLockout: false);
            }
            return RedirectToAction("Products/" + Id, "Home", new { type = x });
        }

        public PartialViewResult _ShowCarts()
        {

            string user = User.Identity.GetUserId();
            var ct = db.CartDetails.Where(x => x.CartID == x.Cart.ID && x.Cart.UserID == user
            && x.Cart.Role != "Deliver" && x.Cart.Role != "Prepare").ToList();
            int maxID = db.Carts.Where(a => a.UserID == user).Max(a => a.ID);
            double sum = 0;
            foreach (var item in ct)
            {
                sum += double.Parse(item.Price.ToString());
            }
            ViewBag.Total = sum;
            var Udate = db.Carts.Where(a => a.UserID == user && a.ID == maxID);
            Udate.FirstOrDefault().Total = sum;
            db.SaveChanges();
            return PartialView(ct);
        }

        public JsonResult Deliver(int cid, int ID)
        {
            int m = 0;
            string user = User.Identity.GetUserId();
            var c = db.Carts.Where(x => x.ID == cid && x.UserID == user).FirstOrDefault();
            if (ID == 1)
                c.Role = "Deliver";
            else
                c.Role = "Prepare";
            m = db.SaveChanges();

            return new JsonResult { Data = m, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

    }
    public class GetSevices
    {

        public List<Category> Categorys { get; set; }

        public List<Region> Regions { get; set; }
    }
}