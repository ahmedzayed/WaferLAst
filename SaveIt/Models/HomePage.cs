﻿using SaveIt.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaveIt.Models
{
    public class HomePage
    {
        public Ws_Cards MainCard { get; set; }
        public List<Ws_Cards> Cards { get; set; }
        public Ws_Video Video { get; set; }
        public List<DonatorContact> DonatorContact { get; set; }
        public Donator DontarById { get; set; }
        public List<DonatorBranch> DonatorBranch { get; set; }
        public List<DonatService> DonatedService { get; set; }

        public List<Ws_Photos>  photos { get; set; }
        public List<Ws_GoldSponsor> GoldSponsors { get; set; }
        public List<Ws_Slider> Sliders { get; set; }
        public List<Donator> Donators { get; set; }
        public List<WebsiteStatistic> WebsiteStatistics { get; set; }
        public List<Advertisment> Advertisments { get; set; }
        public List<Category> Category { get; set; }
        public List<Ws_SocialMedia> Ws_SocialMedia { get; set; }
        public List<Donator> DontarTake { get; set; }
        public List<Donator>FirstCat { get; set; }
        public List<Donator> SacandCat { get; set; }
        public List<Donator> ThrCat { get; set; }
        public List<Category> ListName { get; set; }
        public List<TempDiscountService> TempDiscountService { get; set; }
        public List<Event> Event { get; set; }
        public List<News> News { get; set; }

    }
    public class Cardss
    {
        public Ws_Cards MainCard { get; set; }
        public List<Ws_Cards> Cards { get; set; }


    }
}