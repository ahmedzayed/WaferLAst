﻿var map,
            mLat,
            mLng,
                marker = [],
                loc,
                    mnum = 0;

function initialize() {
    var mapOptions = {
        zoom: 10,
        center: new google.maps.LatLng(30.03937760500134, 30.931320190429688),
        mapTypeId: google.maps.MapTypeId.HYBRID
    };
    map = new google.maps.Map(document.getElementById('mymap'),
        mapOptions
    );



    // add a click event listener to
    // get the lat/lng from click event of map
    google.maps.event.addListener(map, 'click', function (event) {
        mLat = event.latLng.lat();
        mLng = event.latLng.lng();

        document.getElementById('x').value = mLat;
        document.getElementById('y').value = mLng;

        //--------------------------------------marker ---------------
        //remove marker----
        for (var i = 0; i < marker.length; i++) {
            marker[i].setMap(null);
            mnum = marker.length;
        }
        //create marker----
        loc = new google.maps.LatLng(mLat, mLng);
        marker[mnum] = new google.maps.Marker({
            position: loc,
            animation: google.maps.Animation.BOUNCE
        });


        marker[mnum].setMap(map);

    });
}

google.maps.event.addDomListener(window, 'load', initialize);


