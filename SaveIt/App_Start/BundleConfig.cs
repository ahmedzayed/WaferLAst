﻿using System.Web;
using System.Web.Optimization;

namespace SaveIt
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                       "~/Scripts/jquery-2.2.3.min.js",  "~/Scripts/jquery.unobtrusive-ajax.min.js", "~/Scripts/jquery.validate.min.js", "~/Scripts/jquery.validate.unobtrusive.min.js", "~/Scripts/jquery.validate-vsdoc.js", "~/Scripts/additional-methods.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                    "~/Scripts/jquery.js",  "~/Scripts/bootstrap.js", 
                      "~/Scripts/owl.carousel.min.js", "~/Scripts/mousescroll.js", "~/Scripts/smoothScroll.js", "~/Scripts/jquery.prettyPhoto.js",
                       "~/Scripts/jquery.inview.min.js", "~/Scripts/wow.min.js", "~/Scripts/About.js"));
            





                   bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css"
                     , "~/Content/demo.css", "~/Content/animate.min.css", "~/Content/owl.carousel.css", 
                      "~/Content/owl.transitions.css", "~/Content/prettyPhoto.css", "~/Content/main.css", "~/Content/footer-distributed-with-address-and-phones.css",
                      "~/Content/font-awesome.css", "~/Content/responsive.css", "~/Content/agency.css"));
           



                        bundles.Add(new StyleBundle("~/datepicker/css").Include(
                     "~/Content/bootstrap-datepicker.css",
                     "~/Content/bootstrap-datepicker3.css", "~/Content/bootstrap-datetimepicker.css"));

            bundles.Add(new ScriptBundle("~/bundles/datepicker").Include(
                   "~/Scripts/moment.min.js", "~/Scripts/bootstrap-datepicker.js",
                      "~/Scripts/bootstrap-datetimepicker.min.js"));
        }
    }
}
