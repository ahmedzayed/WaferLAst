﻿using System.Configuration;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SaveIt.Startup))]
namespace SaveIt
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            
            app.MapSignalR();
            ConfigureAuth(app);
        }
    }
}
