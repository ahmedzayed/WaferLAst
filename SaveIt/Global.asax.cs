﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace SaveIt
{
    public class MvcApplication : System.Web.HttpApplication
    {
        string _connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
       
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            SqlDependency.Start(_connString);
        }
        public override void Init()
        {
            CultureInfo newCultureDefinition = (CultureInfo)Thread.CurrentThread.CurrentCulture.Clone();
            newCultureDefinition.NumberFormat.PercentPositivePattern = 1;
            newCultureDefinition.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
            Thread.CurrentThread.CurrentCulture = newCultureDefinition;
        }
        protected void Application_End()
        {
            //Stop SQL dependency
            SqlDependency.Stop(_connString);
        }
    }
}
