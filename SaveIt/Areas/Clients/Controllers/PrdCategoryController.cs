﻿using SaveIt.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SaveIt.Areas.Admin.Controllers
{
    //[Authorize(Roles = "Donator")]
    public class PrdCategoryController : Controller
    {
        // GET: Admin/Donators
        private SaveItEntities db = new SaveItEntities();
        public ActionResult Index()
        {
            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);
            var m = Convert.ToInt32(Session["DonatorID"]);
            var Crd = db.ProductsCategories.Where(x => x.IsActive == true && x.DonatorID == m).ToList();
            return View(Crd);
        }
        [HttpPost]
        public ActionResult Edit(ProductsCategory PrdCat)
        {
            try
            {
                if (PrdCat.ID == 0)
                {
                    db.ProductsCategories.Add(PrdCat);
                    db.SaveChanges();
                    TempData["error"] = "تم حفظ التصنيف بنجاح";
                }
                else
                {
                    db.Entry(PrdCat).State = EntityState.Modified;
                    PrdCat.IsActive = true;
                    //PrdCat.DonatorID = Convert.ToInt32(Session["DonatorID"]);
                    db.SaveChanges();
                    TempData["error"] = "تم التعديل بنجاح";
                }
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }


            return RedirectToAction("Index");
        }

        public int Delete(int id)
        {
            var PrdCat = db.ProductsCategories.Find(id);
            if (PrdCat != null)
            {
                PrdCat.IsActive = false;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }

    }
}