﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SaveIt.Entity;
using SaveIt.Models;

namespace SaveIt.Areas.Clients.Controllers
{
    //[Authorize(Roles = "Client")]
    public class ClientControlPanelController : Controller
    {
        SaveItEntities db = new SaveItEntities();
        public ClientControlPanelController()
        {

        }
        private ApplicationUserManager _userManager;

        public ClientControlPanelController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Clients/ClientControlPanel
        public ActionResult Index()
        {
            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);
            var usr = UserManager.FindById(User.Identity.GetUserId());
            var cars = db.Cards.Find(usr.UserID);
            ViewBag.Username = usr.UserName;
            return View(cars);
        }



        [HttpPost]
        public ActionResult ResetUserName( string oldUserName, string newUserName)
        {
            var us = UserManager.FindByName(oldUserName);
            if (us != null)
            {
               // us.Email = newUserName + "@Client.com";
                us.UserName = newUserName;
                var res = UserManager.Update(us);
                if (!res.Succeeded) {
                    TempData["error"] = "يرجي ادخال اسم مستخدم اخر";
                }
            }
            else
            {
                TempData["error"] = "الحساب غير موجود";
            }


            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult ResetPassword( RegisterViewModel reg)
        {
            if (!reg.Password.Equals(reg.ConfirmPassword))
            {
                TempData["error"] = "كلمة المرور غير مطابقة للتاكيد";
                return RedirectToAction("Index");
            }

            var usr = UserManager.FindByName(reg.UserName);
            if (usr != null)
            {
                string resetToken = UserManager.GeneratePasswordResetToken(usr.Id);
                IdentityResult result = UserManager.ResetPassword(usr.Id, resetToken, reg.Password);
                if (result.Succeeded)
                    TempData["error"] = "تم تغيير كلمة المرور بنجاح";
                else
                    TempData["error"] = " لم يتم تغيير كلمة المرور ";

            }
            else
            {
                TempData["error"] = "الحساب غير موجود";
            }

            return RedirectToAction("Index");
        }

    }
}