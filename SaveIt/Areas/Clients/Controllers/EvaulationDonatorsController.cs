﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SaveIt.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SaveIt.Areas.Clients.Controllers
{
    [Authorize(Roles = "Client")]
    public class EvaulationDonatorsController : Controller
    {
        SaveItEntities db = new SaveItEntities();
        public EvaulationDonatorsController()
        {

        }
        private ApplicationUserManager _userManager;

        public EvaulationDonatorsController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Index()
        {

            var usr = UserManager.FindById(User.Identity.GetUserId());
            var donator = db.Donators.Where(c => c.IsActive > 0).ToList();
            ViewBag.userid = usr.UserID;
            return View(donator);
        }
        [HttpGet]
        public ActionResult Test(int id)
        {
            ViewBag.DonatorId = id;
            var don = db.Donators.Find(id);
            ViewBag.name = don.OrgName;
            var quests = db.EvaluationQuestions.Where(c => c.IsDelete != true).ToList();
            return View(quests);
        }
        [HttpPost]
        public ActionResult Test(List<int> Evalution,List<int>id,int DonatorId)
        {
            var usr = UserManager.FindById(User.Identity.GetUserId());
            var answers = new List<EvaluationClient>();
            for (int i = 0; i < id.Count; i++)
            {
                answers.Add(new EvaluationClient()
                {
                    ClientId=usr.UserID,
                    DonatorId= DonatorId,
                  EvaluationQuestionId=id[i],
                  Evaluate=Evalution[i]  
                });
            }
            db.EvaluationClients.AddRange(answers);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}