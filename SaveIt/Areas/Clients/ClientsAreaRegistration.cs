﻿using System.Web.Mvc;

namespace SaveIt.Areas.Clients
{
    public class ClientsAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Clients";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Clients_default",
                "Clients/{controller}/{action}/{id}",
                new { controller="ClientControlPanel", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}