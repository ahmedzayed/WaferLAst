﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SaveIt.Entity;

namespace SaveIt.Areas.Employee.Controllers
{
    [Authorize(Roles = "Employee,Admin")]
    public class DonatorsBranchesController : Controller
    {

        private SaveItEntities db = new SaveItEntities();

        public DonatorsBranchesController()
        {

        }
        private ApplicationUserManager _userManager;

        public DonatorsBranchesController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Index(int? id)
        {
            if (id == null)
                return RedirectToAction("Index", "Donators");
            ViewBag.DonatorID = id;
            var oders = db.DonatorBranches.Where(c => c.DonatorID == id).ToList();
            return View(oders);
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            if(id==0)
                return PartialView(new DonatorBranch());
            var branch = db.DonatorBranches.Find(id);
            return PartialView(branch);
        }
        [HttpPost]
        public ActionResult Edit(DonatorBranch order)
        {
            try
            {
     
                // db.Entry(condetion).State = EntityState.Modified;
                db.DonatorBranches.AddOrUpdate(order);
                db.SaveChanges();
                TempData["error"] = "تم حفظ التغييرات بنجاح";

            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }
            return RedirectToAction("Index",new { id=order.DonatorID});
        }


        public int Delete(int id)
        {
            try
            {
                var order = db.DonatorBranches.Find(id);
                if (order != null)
                {
                    db.DonatorBranches.Remove(order);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }

    }
}