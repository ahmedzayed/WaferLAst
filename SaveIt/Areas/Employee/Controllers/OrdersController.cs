﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SaveIt.Entity;
using SaveIt.Models;

namespace SaveIt.Areas.Employee.Controllers
{
    [Authorize(Roles = "Admin")]
    public class OrdersController : Controller
    {
        private SaveItEntities db = new SaveItEntities();

        public OrdersController()
        {

        }
        private ApplicationUserManager _userManager;

        public OrdersController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public ActionResult Index()
        {
            var donators = db.Donators.Where(c=>c.OrderServices.Any(cx=>cx.Status==0)).ToList();
            return View(donators);
        }
        public int RequestReplay(int id, int type)
        {
            try
            {
                var order = db.OrderServices.Find(id);
                if (order != null)
                {
                    order.Status = type;
                    db.OrderServices.AddOrUpdate(order);
                    db.SaveChanges();
                    if (type == 1)
                    {
                        var services = new DonatService
                        {
                            DonatorID = order.DonatorID,
                            Discount = order.Discount,
                            Service = order.Service
                        };
                        db.DonatServices.Add(services);
                        db.SaveChanges();
                    }
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }
    }
}