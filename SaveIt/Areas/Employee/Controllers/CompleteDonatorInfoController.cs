﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SaveIt.Entity;

namespace SaveIt.Areas.Employee.Controllers
{
    [Authorize(Roles = "Employee,Admin")]
    public class CompleteDonatorInfoController : Controller
    {
        private SaveItEntities db = new SaveItEntities();
        public ActionResult Index()
        {
            var don = db.Donators.Where(c => c.IsActive == 1 || c.IsActive == 2).ToList();
            return View(don);
        }


        [HttpPost]
        public ActionResult CreateCondition(int DonatorID, List<string> Condition)
        {
            try
            {
                foreach (string t in Condition)
                {
                    var condetion = new CondetionService()
                    {
                        DonatorID = DonatorID,
                        Condition = t,
                     
                    };
                    db.CondetionServices.Add(condetion);
                    db.SaveChanges();
                }


                TempData["error"] = "تم الحفظ  بنجاح";
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }
            return RedirectToAction("Details","Donators", new {id = DonatorID});
        }



        [HttpPost]
        public ActionResult EditCondition(CondetionService condetion)
        {
            try
            {
                   // db.Entry(condetion).State = EntityState.Modified;
                    db.CondetionServices.AddOrUpdate(condetion); 
                    db.SaveChanges();
                    TempData["error"] = "تم التعديل بنجاح";
                
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }
            return RedirectToAction("Details","Donators", new {id = condetion.DonatorID});
        }


        public int DeleteCondition(int id)
        {
            try
            {
                var condetion = db.CondetionServices.Find(id);
                if (condetion != null)
                {
                    db.CondetionServices.Remove(condetion   );
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }

        [HttpPost]
        public ActionResult CreateFeature(int? DonatorID, List<string> Feature)
        {
          
            try
            {
                foreach (string t in Feature)
                {
                    var feature = new FeatureService()
                    {
                        DonatorID = DonatorID,
                        Feature = t
                    };
                    db.FeatureServices.Add(feature);
                    db.SaveChanges();
                }

              
                    TempData["error"] = "تم الحفظ  بنجاح";
            
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }
            return RedirectToAction("Details", "Donators", new { id = DonatorID });
        }

        [HttpPost]
        public ActionResult EditFeature(int? DonatorID, string Feature, int ID)
        {
            var feature=new FeatureService()
            {
                DonatorID = DonatorID,
                Feature = Feature,
                ID = ID
            };
            try
            {
               
                    db.FeatureServices.AddOrUpdate(feature); 
                    db.SaveChanges();
                    TempData["error"] = "تم التعديل بنجاح";
                
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }
            return RedirectToAction("Details", "Donators", new { id = feature.DonatorID });
        }


        public int DeleteFeature(int id)
        {
            try
            {
                var feature = db.FeatureServices.Find(id);
                if (feature != null)
                {
                    db.FeatureServices.Remove(feature);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }
        [HttpPost]
        public ActionResult CreateWork(List<string> ValidFrom, List<string> validto, int DonatorID)
        {
            try
            {
                for (int i = 0; i < ValidFrom.Count; i++)
                {

                    var calender = new WorkCalender()
                    {
                        DonatorID = DonatorID,
                        ValidFrom = ValidFrom[i],
                        validto = validto[i]
                    };
                    db.WorkCalenders.Add(calender);
                    db.SaveChanges();
                }
                    TempData["error"] = "تم الحفظ  بنجاح";
               
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }
            return RedirectToAction("Details", "Donators", new { id = DonatorID });
        }
     [HttpPost]
        public ActionResult EditWork(WorkCalender calender)
        {
            try
            {
               
                    db.WorkCalenders.AddOrUpdate(calender);
                    db.SaveChanges();
                    TempData["error"] = "تم التعديل بنجاح";
                
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }
            return RedirectToAction("Details", "Donators", new { id = calender.DonatorID });
        }


        public int DeleteWork(int id)
        {
            try
            {
                var calender = db.WorkCalenders.Find(id);
                if (calender != null)
                {
                    db.WorkCalenders.Remove(calender);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }
        [HttpPost]
        public ActionResult creategeneralcon(int DonatorID,List<string>Condition  )
        {
            try
            {
                foreach (string t in Condition)
                {
                    if (!string.IsNullOrWhiteSpace(t))
                    {
                        var condetion = new CondetionService()
                        {
                            DonatorID = DonatorID,
                            Condition = t,

                        };
                        db.CondetionServices.Add(condetion);
                        db.SaveChanges();  
                    }
                 
                }

                TempData["error"] = "تم الحفظ  بنجاح";
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }
            return RedirectToAction("Details", "Donators", new { id = DonatorID });
        }
	}
}