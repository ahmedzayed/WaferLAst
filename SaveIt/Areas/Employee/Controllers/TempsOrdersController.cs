﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SaveIt.Entity;
using SaveIt.Models;

namespace SaveIt.Areas.Employee.Controllers
{
    [Authorize(Roles = "Admin")]
    public class TempsOrdersController : Controller
    {
        private SaveItEntities db = new SaveItEntities();

        public TempsOrdersController()
        {

        }
        private ApplicationUserManager _userManager;

        public TempsOrdersController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public ActionResult Index()
        {
            var donators = db.Donators.Where(c=>c.TempDiscountServices.Any(cx=>cx.IsOrder==true&&cx.IsAccepted==null)).ToList();
            return View(donators);
        }
        public int RequestReplay(int id, bool type)
        {
            try
            {
                var order = db.TempDiscountServices.Find(id);
                if (order != null)
                {
                    order.IsAccepted = type;
                    db.TempDiscountServices.AddOrUpdate(order);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }
    }
}