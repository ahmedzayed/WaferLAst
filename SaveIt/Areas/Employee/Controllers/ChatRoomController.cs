﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SaveIt.Entity;
using SaveIt.Models;
using SaveIt.ModelView;

namespace SaveIt.Areas.Employee.Controllers
{
    // [Authorize(Roles = "Admin")]
    public class ChatRoomController : Controller
    {
        private SaveItEntities db = new SaveItEntities();

        public ChatRoomController()
        {

        }
        private ApplicationUserManager _userManager;

        public ChatRoomController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public ActionResult Index()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            var demoned = db.DemondChats.Where(c => c.EmpID == user.UserID).OrderByDescending(c => c.ID).ToList();

            return View(demoned);
        }
        //public ActionResult Cat()
        //{
        //    int reciverid = 0, senderid = 0;
        //    var messageRepository = new MessagesRepository();
        //    return PartialView(messageRepository.GetAllMessages(senderid, reciverid));
        //}
        public ActionResult GetAllNotifications()
        {
            var messageRepository = new MessagesRepository();
            var demond = messageRepository.GetAllNotifications();
            var demos = new List<DemondChating>();
            foreach (var c in demond)
            {
                try
                {
                    var donator = db.Donators.Find(c.UserID);
                    var find = db.Cards.Find(c.UserID);
                    if (donator != null || find != null)
                    {
                        var de = new DemondChating
                        {
                            ID = c.ID,
                            UserID = c.UserID,
                            Create = c.CreateDate,
                            Type = c.UserType,
                            UserName = c.UserType == "Donator" ? donator.OwnName : find.Name
                        };
                        demos.Add(de);
                    }
                }
                catch (Exception) { }
            }
            return PartialView(demos);
        }

    }
}