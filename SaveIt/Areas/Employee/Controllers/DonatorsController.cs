﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SaveIt.Entity;
using SaveIt.Infrastructure;
using SaveIt.Models;

namespace SaveIt.Areas.Employee.Controllers
{
     [Authorize(Roles = "Employee,Admin")]
    public class DonatorsController : Controller
    {
        private SaveItEntities db = new SaveItEntities();

          public DonatorsController()
        {

        }
        private ApplicationUserManager _userManager;

        public DonatorsController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public ActionResult Index(int? Region)
        {
            ViewBag.Region = new SelectList(db.Regions, "ID", "Name", Region);

            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);

            var donators = db.Donators.Where(c=>Region==null||c.RegionId==Region).ToList();
           
            return View(donators);
        }
        [EncryptedActionParameter]

        public ActionResult Evaluation(int Id)
        {
            ViewBag.name = db.Donators.Find(Id).OrgName;
            var evaluate = db.EvaluationClients.Where(c => c.DonatorId == Id).GroupBy(c=>c.Card).ToList();
            return View(evaluate);
        }
        [HttpPost]
     public ActionResult SetSocialMedia(DonatorContact contact)
        {
          
            try
            {
                db.DonatorContacts.AddOrUpdate(contact);
                db.SaveChanges();
                TempData["error"] = "تم الاضافة بنجاح";
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ادخال البيانات صحيحة";
            }
            return RedirectToAction("Details", new { id = contact.DonatorID });
        }
        public void DeleteSocial(int id)
        {
               var con= db.DonatorContacts.Find(id);
                db.DonatorContacts.Remove(con);
                db.SaveChanges();
          }

        public void DeleteTemp(int id)
        {
            var con = db.TempDiscountServices.Find(id);
            db.TempDiscountServices.Remove(con);
            db.SaveChanges();
        }
        [EncryptedActionParameter]

        public ActionResult Details(int Id)
        {
            if (Id == 0)
                return RedirectToAction("Index");
            var don = db.Donators.Include(c=>c.DonatServices.Select(e=>e.TempDiscountServices)).FirstOrDefault(c=>c.ID==Id);
            if(don==null)
                return RedirectToAction("Index");
            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);
            ViewBag.CatID = new SelectList(db.Categorys, "ID", "Name",don.CatID);
            ViewBag.RegionId = new SelectList(db.Regions, "ID", "Name",don.RegionId);
            ViewBag.cons = db.GeneralConditions.ToList();

            return View(don);
        }
        [HttpGet]
        [EncryptedActionParameter]

        public ActionResult Edits(int Id)
        {
            var donator = new Donator();
            if (Id != 0)
            {
                ViewBag.tit = "تعديل معلومات المانح";
                donator = db.Donators.Find(Id);
                ViewBag.CatID = new SelectList(db.Categorys, "ID", "Name",donator.CatID);
                ViewBag.RegionId = new SelectList(db.Regions, "ID", "Name", donator.RegionId);
            }
            else
            {
                ViewBag.tit = "انشاء مانح جديد";
                ViewBag.CatID = new SelectList(db.Categorys, "ID", "Name");
                ViewBag.RegionId = new SelectList(db.Regions, "ID", "Name");
            }
             

            return View(donator);
        }
        public ActionResult Edit()
        {
            return RedirectToAction("Edits");
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(Donator donator,string versdate, string findate, string visdate, string condate, string disdate)
        {
            ViewBag.CatID = new SelectList(db.Categorys, "ID", "Name", donator.CatID);
            try
            {
               // donator.Versiondate =Convert.ToDateTime(versdate);
               //donator.ContractDate = Convert.ToDateTime(condate);
               // donator.VisiteDate = Convert.ToDateTime(visdate);
               // donator.finishDate = Convert.ToDateTime(findate);
               // donator.DiscountDate = Convert.ToDateTime(disdate);

                if (donator.ContractDate!=null&&donator.finishDate != null &&
                    donator.ContractDate < donator.finishDate)
                {
                    var dd = (DateTime)donator.finishDate - (DateTime)donator.ContractDate;
                    var ddd = dd.Days;
                    if (ddd > 365)
                    {
                        var ye = ddd/365;
                        var days = ddd - ye*365;
                        if (days > 30)
                        {
                            int mo = days / 30;
                            int day = days - mo * 30;
                            donator.Peroid =ye+" عام -"+ mo + " شهر -" + day + "  يوم";
                        } 
                    }
                    else if (ddd > 30)
                    {
                        int mo = ddd/30;
                        int day = ddd - mo*30;
                        donator.Peroid = mo + " شهر -" + day + "  يوم";
                    }
                    else
                    {
                        donator.Peroid = ddd + " يوم";
                    }
                }
                else
                {
                    ModelState.AddModelError("", "يرجي ادخال تاريخ بداية العقد وتاريخ نهاية العقد بالطرق الصحيحة");
                    return View("Edits",donator);
                }
                if (donator.ID == 0)
                {
                   
                    donator.IsActive = 0;
                    db.Donators.Add(donator);
                    db.SaveChanges();
                    TempData["error"] = "تم حفظ المانح بنجاح";
                }
                else
                {
                    db.Donators.AddOrUpdate(donator); 
                    db.SaveChanges();
                    TempData["error"] = "تم التعديل بنجاح";
                }
            }
            catch (Exception)
            {
              
                ModelState.AddModelError("", "يرجي ملئ كل البيانات");
                return View("Edits", donator);
            }
          
                return RedirectToAction("Index");
 
        }
        
        public int Delete(int id)
        {
            try
            {
                var donator = db.Donators.Find(id);
                if (donator != null)
                {
                if (donator.Username != null)
                {
                    var usr = UserManager.FindByName(donator.Username);
                    if (usr != null)
                    {
                        if (UserManager.GetRoles(usr.Id).Any())
                        {
                            var usrRoles = UserManager.GetRoles(usr.Id);
                            UserManager.RemoveFromRoles(usr.Id, usrRoles.ToArray());
                        }
                        UserManager.Delete(usr);
                    }
                }
                   
                    db.Donators.Remove(donator);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }

        [HttpPost]
        public ActionResult TempDiscount(TempDiscountService model)
        {
            model.IsAccepted = true;
            model.IsOrder = false;
            db.TempDiscountServices.AddOrUpdate(model);
            if (db.SaveChanges() > 0)
                TempData["error"] = "تم اضافة الخصم";
            else
                TempData["error"] = "يرجي اعادة كتابة المعلومات بالطريقة الصحيحة";
            return RedirectToAction("Details", new { id = model.DonatorId });
        }

         [HttpPost]
         public ActionResult createAcountDonator(int DonatorID,RegisterViewModel reg)
         {
             if (string.IsNullOrWhiteSpace(reg.UserName))
             {
                 TempData["error"] = "يرجي ادخال اسم مستخدم صحيح";
                 return RedirectToAction("Details", new { id = DonatorID });
             }
             if (!reg.Password.Equals(reg.ConfirmPassword))
             {
                 TempData["error"] = "كلمة المرور غير مطابقة للتاكيد";
                 return RedirectToAction("Details", new { id = DonatorID });
             }
             var donator = db.Donators.Find(DonatorID);
             var usr = UserManager.FindByName(reg.UserName);
             if (usr == null)
             {
                 // int userid = MAx.UserID(UserManager);
                 var uu = UserManager.Create(new ApplicationUser
                 {
                     UserName = reg.UserName,
                     FullName = donator.OwnName,
                     Email = reg.UserName + "@Donator.com",
                     Type = 3,
                     UserID = DonatorID
                 }, reg.Password);
                 if (uu.Succeeded)
                 {
                     var us = UserManager.FindByName(reg.UserName);
                     UserManager.AddToRole(us.Id, "Donator");
                     donator.Username = reg.UserName;
                     db.Donators.AddOrUpdate(donator);
                     db.SaveChanges();
                     TempData["error"] = "تم انشاء الحساب بنجاح";   
                 }
                 else
                 {
                     TempData["error"] = "يرجي كتابة كلمة المرور اكبر من 6 احرف";   
                 }
             }
             else
             {

                 TempData["error"] = "اسم المستخدم موجود بالفعل يرجي اختيار اسم اخر";
                // return RedirectToAction("Details", new { id = DonatorID });   
             }
             return RedirectToAction("Details", new { id = DonatorID });
         }

         [HttpPost]
         public ActionResult ResetUserName(int DonatorID, string oldUserName, string newUserName)
         {
             var us = UserManager.FindByName(oldUserName);
             if (us != null)
             {
                 us.Email = newUserName + "@Donator.com";
                 us.UserName = newUserName;
                 var res = UserManager.Update(us);
                 if (res.Succeeded)
                 {
                     var donat = db.Donators.Find(DonatorID);
                     donat.Username = newUserName;
                     db.Donators.AddOrUpdate(donat);
                     db.SaveChanges();
                     TempData["error"] = "تم تغيير اسم المستخدم بنجاح";
                 }
                 else
                 {
                     TempData["error"] = "يرجي ادخال اسم مستخدم اخر";
                 }
             }
             else
             {
                 TempData["error"] = "الحساب غير موجود";
             }
     

             return RedirectToAction("Details", new { id = DonatorID });
         }
         [HttpPost]
         public ActionResult ResetPassword(int DonatorID, RegisterViewModel reg)
         {
             if (!reg.Password.Equals(reg.ConfirmPassword))
             {
                 TempData["error"] = "كلمة المرور غير مطابقة للتاكيد";
                 return RedirectToAction("Details", new { id = DonatorID });
             }
             
             var usr = UserManager.FindByName(reg.UserName);
             if (usr != null)
             {
                 string resetToken = UserManager.GeneratePasswordResetToken(usr.Id);
                 IdentityResult result = UserManager.ResetPassword(usr.Id, resetToken, reg.Password);
                 if (result.Succeeded)
                      TempData["error"] = "تم تغيير كلمة المرور بنجاح";
                 else
                     TempData["error"] = " لم يتم تغيير كلمة المرور ";

             }
             else
             {
                 TempData["error"] = "الحساب غير موجود"; 
             }

             return RedirectToAction("Details", new { id = DonatorID });
         }

        [HttpPost]
        public ActionResult DonatService(DonatService serv)
        {
            try
            {
                    db.DonatServices.AddOrUpdate(serv);
                    db.SaveChanges();
                    TempData["error"] = "تم التعديل بنجاح";
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }

            return RedirectToAction("Details", new { id = serv.DonatorID });
        }
        [HttpPost]
        public ActionResult CreateDonatService(int DonatorID, List<string> Service, List<int> Discount,bool IsGeneral)
        {
            try
            {
                for (int i = 0; i < Service.Count; i++)
                {
                    var serv=new DonatService()
                    {
                        DonatorID = DonatorID,
                        Discount = Discount[i],
                        Service = Service[i],
                        IsGeneral=IsGeneral
                    };
                    db.DonatServices.Add(serv);
                    db.SaveChanges(); 
                }
                    TempData["error"] = "تم حفظ  بنجاح";
             
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }

            return RedirectToAction("Details", new { id = DonatorID });
        }

        public int DeleteDonserv(int id)
        {
            try
            {
                var Services = db.DonatServices.Find(id);
                if (Services != null)
                {
                    db.DonatServices.Remove(Services);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }
        [HttpPost]
        public ActionResult CreateDonatedService(int DonatorID, List<string> Service, List<int> Discount)
        {
            try
            {

                for (int i = 0; i < Service.Count; i++)
                {
                    var serv = new DonatedService()
                    {
                        DonatorID = DonatorID,
                        Discount = Discount[i],
                        Service = Service[i]
                    };
                    db.DonatedServices.Add(serv);
                    db.SaveChanges();
                }
                    TempData["error"] = "تم التعديل بنجاح";
                
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }

            return RedirectToAction("Details", new { id = DonatorID });
        }
  [HttpPost]
        public ActionResult DonatedService(DonatedService serv)
        {
            try
            {

                db.DonatedServices.AddOrUpdate(serv); 
                    db.SaveChanges();
                    TempData["error"] = "تم التعديل بنجاح";
                
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }

            return RedirectToAction("Details", new { id = serv.DonatorID });
        }

        public int DeleteDonedserv(int id)
        {
            try
            {
                var Services = db.DonatedServices.Find(id);
                if (Services != null)
                {
                    db.DonatedServices.Remove(Services);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }


        [HttpPost]
        public ActionResult updatePhoto(int PID, HttpPostedFileBase uploadfile)
        {
            if (uploadfile == null)
            {
                TempData["error"] = "من فضلك اضف صوره";
                return RedirectToAction("Details", new { id = PID });
            }

            try
            {
                var pg = db.Donators.Find(PID);
                if (pg.Photo != null)
                {
                    try
                    {
                        string filePath = Server.MapPath(Url.Content("~/" + pg.Photo));
                        System.IO.File.Delete(filePath);
                    }
                    catch (Exception)
                    {

                    }
                 
                }
                string fileName = Guid.NewGuid() + Path.GetExtension(uploadfile.FileName);
                uploadfile.SaveAs(Path.Combine(Server.MapPath("~/Images"), fileName));
                pg.Photo = "/Images/" + fileName;
                db.Donators.AddOrUpdate(pg);
                db.SaveChanges();
                TempData["error"] = "تم اضافة الصورة بنجاح";
            }
            catch
            {
                TempData["error"] = "من فضلك ان لايزيد الصورة عن 2 ميجا";
            }

            return RedirectToAction("Details", new { id = PID });
        }

         public ActionResult ChangeLocation(int id, string lat, string lang)
         {
             var don = db.Donators.Find(id);
             don.LocatLat = lat;
             don.LocatLang = lang;
             db.Donators.AddOrUpdate(don);
             db.SaveChanges();
             return RedirectToAction("Details", new { id = id });
         }

        public int RequestReplay(int id, int type)
        {
            try
            {
                var order = db.OrderServices.Find(id);
                if (order != null)
                {
                    order.Status = type;
                    db.OrderServices.AddOrUpdate(order);
                    db.SaveChanges();
                    if (type == 1)
                    {
                        var services = new DonatService
                        {
                            DonatorID = order.DonatorID,
                            Discount = order.Discount,
                            Service = order.Service
                        };
                        db.DonatServices.Add(services);
                        db.SaveChanges();
                    }
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }
    }
}