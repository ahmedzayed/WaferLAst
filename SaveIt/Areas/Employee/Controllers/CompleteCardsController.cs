﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SaveIt.Entity;
using SaveIt.ModelView;
using SaveIt.Models;

namespace SaveIt.Areas.Employee.Controllers
{
    //[Authorize(Roles = "Employee,Admin")]
    public class CompleteCardsController : Controller
    {
        private SaveItEntities db = new SaveItEntities();
        public CompleteCardsController()
        {

        }
        private ApplicationUserManager _userManager;

        public CompleteCardsController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: /Employee/CompleteCards/
        public ActionResult Index()
        {

            var users = UserManager.Users.Where(c => c.Type == 2).ToList();
            ViewBag.CardCategoryId = new SelectList(db.CardCategories.ToList(), "Id", "Name");
            ViewBag.UserID = new SelectList(users, "UserID", "FullName");

            var cards = db.Cards.Where(c => c.Status == 0).ToList();
            //   var cards = db.Cards.ToList();
            return View(cards);
        }


        public int DeleteCard(int id)
        {
            var card = db.Cards.Find(id);
            card.Status = -1;
            db.Cards.AddOrUpdate(card);
           return db.SaveChanges();
        }

        public ActionResult GetCode(int id)
        {
            var cate = db.CardCategories.Find(id);
            if (db.Cards.Any(c => c.CardCategoryId == id && c.Code == cate.EndCode))
                return Content("تم امتلاء هذة المجموعة يرجي تحديد مجموعة اخري!");
            var card = db.Cards.Where(c => c.CardCategoryId == id).OrderByDescending(c => c.Code).FirstOrDefault();
            if (card == null)
            {
                ViewBag.Code = cate.StartCode;
                ViewBag.SubCode = cate.Code;
            }
            else
            {
                var nextcode = (card.Code ?? cate.StartCode) + 1;
                ViewBag.Code = nextcode;
                ViewBag.SubCode = cate.Code;
            }
            ViewBag.Price = cate.Value;
            return PartialView();
        }

        public ActionResult Create()
        {
            var users = UserManager.Users.Where(c => c.Type == 2).ToList();
            ViewBag.CardCategoryId = new SelectList(db.CardCategories.ToList(), "Id", "Name");
            ViewBag.UserID = new SelectList(users, "UserID", "FullName");
            return View();
        }
        [HttpPost]
        public ActionResult Create(DemondCard card, Card Info)
        {


            Info.Status = 1;
            Info.EmpID = UserManager.FindById(User.Identity.GetUserId()).UserID;
            Info.CreateDate = DateTime.Now;
            Info.Agent = UserManager.Users.FirstOrDefault(c => c.UserID == Info.UserID).FullName;
            if (Info.FinishDate != null && Info.StartDate != null)
            {
                var dd = (DateTime)Info.FinishDate - (DateTime)Info.StartDate;
                var ddd = dd.Days;
                if (ddd > 365)
                {
                    var ye = ddd / 365;
                    var days = ddd - ye * 365;
                    if (days > 30)
                    {
                        int mo = days / 30;
                        int day = days - mo * 30;
                        Info.Peroud = ye + " عام -" + mo + " شهر -" + day + "  يوم";
                    }
                }
                else if (ddd > 30)
                {
                    int mo = ddd / 30;
                    int day = ddd - mo * 30;
                    Info.Peroud = mo + " شهر -" + day + "  يوم";
                }
                else
                {
                    Info.Peroud = ddd + " يوم";
                }
            }
            if (Info.Email == null)
                Info.Email = " ";
            if (Info.Phone == null)
                Info.Phone = " ";
            Info = db.Cards.Add(Info);
            var ind = db.SaveChanges();
            if (ind > 0)
            {
                if (card.Email == null || card.Username == null)
                {
                    return RedirectToAction("Complete");
                }
                if (UserManager.FindByEmail(card.Email) == null && UserManager.FindByName(card.Username) == null)
                {
                    var usr = new ApplicationUser
                    {
                        UserName = card.Username,
                        Email = card.Email,
                        Type = 4,
                        FullName = card.Name,
                        UserID = Info.ClientID
                    };
                    var suc = UserManager.Create(usr, card.Password);
                    if (!suc.Succeeded)
                    {
                        db.Cards.Remove(Info);
                        db.SaveChanges();

                        ModelState.AddModelError("", "يرجي اخال الكلمة المرورو اكبر من 6 احرف");
                        //ModelState.AddModelError("", suc.Errors.ElementAt(0));
                    }
                    else
                    {
                        var user = UserManager.FindByName(usr.UserName);
                        UserManager.AddToRole(user.Id, "Client");
                        return RedirectToAction("Complete");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "البريد الاليكتروني او اسم المستخدم متواجد بالفعل يرجي تغييرهم");
                }
            }

            ModelState.AddModelError("", "يرجي ادخال كل البيانات بالطريقة الصحيحة");
            return View(card);
        }


        public ActionResult Complete(int? id)
        {
            var cards = db.Cards.Where(c => c.Status == 2 || c.Status == 1).ToList();
            ViewBag.CategoryId = new SelectList(db.CardCategories.ToList(), "Id", "Name");
            if (Request.IsAjaxRequest())
            {
                return PartialView("list1",cards.Where(c => id == null || c.CardCategoryId == id).ToList());
            }
         
            return View(cards);
        }
        public ActionResult CompleteAccept(int? id, int? type)
        {
            if (id != null && type != null)
            {
                var card = db.Cards.Find(id);
                card.Status = (int)type;
                db.SaveChanges();
            }
            return RedirectToAction("Completed");
        }

        public ActionResult Completing(int?id)
        {
            var cards = db.Cards.Where(c => c.Status != 2&&c.Status!=-1).ToList();

            ViewBag.CategoryId = new SelectList(db.CardCategories.ToList(), "Id", "Name");
            if (Request.IsAjaxRequest())
            {
                return PartialView("list2", cards.Where(c => id == null || c.CardCategoryId == id).ToList());
            }
            return View(cards);
        }
        public ActionResult Completed(int?id)
        {
            var cards = db.Cards.Where(c => c.Status == 2).ToList();

            ViewBag.CategoryId = new SelectList(db.CardCategories.ToList(), "Id", "Name");
            if (Request.IsAjaxRequest())
            {
                return PartialView("list3", cards.Where(c => id == null || c.CardCategoryId == id).ToList());
            }
            return View(cards);
        }
        [HttpPost]
        public ActionResult CompCard(Card card)
        {


            card.Status = 1;
            card.EmpID = UserManager.FindById(User.Identity.GetUserId()).UserID;
            card.UserID = card.EmpID;
            card.CreateDate = DateTime.Now;
            if (card.FinishDate != null && card.StartDate != null)
            {
                var dd = (DateTime)card.FinishDate - (DateTime)card.StartDate;
                var ddd = dd.Days;
                if (ddd > 365)
                {
                    var ye = ddd / 365;
                    var days = ddd - ye * 365;
                    if (days > 30)
                    {
                        int mo = days / 30;
                        int day = days - mo * 30;
                        card.Peroud = ye + " عام -" + mo + " شهر -" + day + "  يوم";
                    }
                }
                else if (ddd > 30)
                {
                    int mo = ddd / 30;
                    int day = ddd - mo * 30;
                    card.Peroud = mo + " شهر -" + day + "  يوم";
                }
                else
                {
                    card.Peroud = ddd + " يوم";
                }

                db.Cards.AddOrUpdate(card);
                if (db.SaveChanges() > 0)
                    return RedirectToAction("Complete");
                // return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }
    }
}