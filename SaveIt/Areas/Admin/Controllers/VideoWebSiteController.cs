﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using SaveIt.Entity;

namespace SaveIt.Areas.Admin.Controllers
{
     [Authorize(Roles = "Admin")]
    public class VideoWebSiteController : Controller
    {
        private SaveItEntities db = new SaveItEntities();
        public ActionResult Index()
        {

            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);

            var videos = db.Ws_Video.ToList();
            return View(videos);
        }
        [HttpPost]
        public ActionResult Edit(Ws_Video video)
        {
            try
            {
                video.VideoPath = geturl(video.VideoPath);
                if (video.ID == 0)
                {

                    db.Ws_Video.Add(video);
                    db.SaveChanges();
                    TempData["error"] = "تم حفظ  بنجاح";
                }
                else
                {
                    db.Ws_Video.AddOrUpdate(video);
                    db.SaveChanges();
                    TempData["error"] = "تم التعديل بنجاح";
                }
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }


            return RedirectToAction("Index");
        }

         public string geturl(string url)
         {
             if (Regex.IsMatch(url, "https://www.youtube.com/embed"))
             {
                 return url;
             }
             const string pattern = @"(?:https?:\/\/)?(?:www\.)?(?:(?:(?:youtube.com\/watch\?[^?]*v=|youtu.be\/)([\w\-]+))(?:[^\s?]+)?)";
             const string replacement = "http://www.youtube.com/embed/$1";
             var rgx = new Regex(pattern);
             var result = rgx.Replace(url, replacement);
             return result;
         }
        public int Delete(int id)
        {
            try
            {
                var video = db.Ws_Video.Find(id);
                if (video != null)
                {
                    db.Ws_Video.Remove(video);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }
	}
}