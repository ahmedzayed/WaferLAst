﻿using SaveIt.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SaveIt.Areas.Admin.Controllers
{
    public class EventTypeController : Controller
    {
        private SaveItEntities db = new SaveItEntities();
        public ActionResult Index()
        {

            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);

            var eventtype = db.EventTypes.ToList();
            return View(eventtype);
        }
        [HttpPost]
        public ActionResult Edit(EventType eventtype)
        {

            try
            {


                if (eventtype.Id == 0)
                {
                    db.EventTypes.Add(eventtype);
                    db.SaveChanges();
                    TempData["error"] = "تم حفظ التصنيف بنجاح";
                }
                else
                {
                    db.Entry(eventtype).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["error"] = "تم التعديل بنجاح";
                }
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }


            return RedirectToAction("Index");
        }
        public int Delete(int id)
        {
            try
            {
                var eventtype = db.EventTypes.Find(id);
                if (eventtype != null)
                {
                    db.EventTypes.Remove(eventtype);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }
    }
}