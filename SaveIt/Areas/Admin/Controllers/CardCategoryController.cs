﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SaveIt.Entity;

namespace SaveIt.Areas.Admin.Controllers
{
     [Authorize(Roles = "Admin")]
    public class CardCategoryController : Controller
    {
        private SaveItEntities db = new SaveItEntities();
        public ActionResult Index()
        {

            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);

            var categorys = db.CardCategories.ToList();
            return View(categorys);
        }
        [HttpPost]
        public ActionResult Edit(CardCategory category)
        {
            try
            {
                if (category.Id == 0)
                {

                    db.CardCategories.Add(category);
                    db.SaveChanges();
                    TempData["error"] = "تم حفظ  بنجاح";
                }
                else
                {
                    db.CardCategories.AddOrUpdate(category);
                    db.SaveChanges();
                    TempData["error"] = "تم التعديل بنجاح";
                }
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }


            return RedirectToAction("Index");
        }
        public int Delete(int id)
        {
            try
            {
                var category = db.CardCategories.Find(id);
                if (category != null)
                {
                    db.CardCategories.Remove(category);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }
	}
}