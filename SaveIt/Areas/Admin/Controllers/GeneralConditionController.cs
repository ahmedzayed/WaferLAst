﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SaveIt.Entity;

namespace SaveIt.Areas.Admin.Controllers
{
     [Authorize(Roles = "Admin")]
    public class GeneralConditionController : Controller
    {
        private SaveItEntities db = new SaveItEntities();
        public ActionResult Index()
        {
          
            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);

            var generalConditions = db.GeneralConditions.ToList();
            return View(generalConditions);
        }
        [HttpPost]
        public ActionResult Edit(GeneralCondition generalCondition)
        {
            try
            {
                if (generalCondition.ID == 0)
                {

                    db.GeneralConditions.Add(generalCondition);
                    db.SaveChanges();
                    TempData["error"] = "تم حفظ  بنجاح";
                }
                else
                {
                    db.Entry(generalCondition).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["error"] = "تم التعديل بنجاح";
                }
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }


            return RedirectToAction("Index");
        }
        public int Delete(int id)
        {
            try
            {
                var generalCondition = db.GeneralConditions.Find(id);
                if (generalCondition != null)
                {
                    db.GeneralConditions.Remove(generalCondition);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }
    }
}