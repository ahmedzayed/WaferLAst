﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SaveIt.Entity;

namespace SaveIt.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ImageWebSiteController : Controller
    {
        private SaveItEntities db = new SaveItEntities();
        public ActionResult Index()
        {

            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);

            var photoss = db.Ws_Photos.ToList();
            return View(photoss);
        }
        [HttpPost]
        public ActionResult Edit(Ws_Photos photos, HttpPostedFileBase Photo)
        {
            try
            {
                if (Photo != null)
                {
                    try
                    {
                        if (photos.PhotoPath != null)
                        {
                            try
                            {
                                string filePath = Server.MapPath(Url.Content("~/" + photos.PhotoPath));
                                System.IO.File.Delete(filePath);
                            }
                            catch (Exception) { }
                        }
                        string fileName = Guid.NewGuid() + Path.GetExtension(Photo.FileName);
                        Photo.SaveAs(Path.Combine(Server.MapPath("~/Images"), fileName));
                        photos.PhotoPath = "/Images/" + fileName;
                    }
                    catch (Exception) { }
                }
                if (photos.PhotoPath != null)
                    if (photos.ID == 0)
                    {

                        db.Ws_Photos.Add(photos);
                        db.SaveChanges();
                        TempData["error"] = "تم حفظ  بنجاح";
                    }
                    else
                    {
                        db.Ws_Photos.AddOrUpdate(photos);
                        db.SaveChanges();
                        TempData["error"] = "تم التعديل بنجاح";
                    }
                else
                    TempData["error"] = "يرجي اختيار صورة";
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }


            return RedirectToAction("Index");
        }
        public int Delete(int id)
        {
            try
            {
                var photos = db.Ws_Photos.Find(id);
                if (photos != null)
                {
                    db.Ws_Photos.Remove(photos);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }
    }
}