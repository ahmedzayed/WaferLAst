﻿using SaveIt.Entity;
using SaveIt.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SaveIt.Areas.Admin.Controllers
{
    public class EventController : Controller
    {
        private SaveItEntities db = new SaveItEntities();
        // GET: Admin/Event
        public ActionResult Index()
        {

            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);

            var Ev = db.Events.ToList();
            ViewBag.Regions = new SelectList(db.Regions.ToList(), "ID", "Name");
            ViewBag.Eventtype = new SelectList(db.EventTypes.ToList(), "Id", "Name");
            return View(Ev);
        }
        [HttpPost]
        public ActionResult Edit(Event Ev, HttpPostedFileBase file, int? EvId)
        {

            try
            {
                if (EvId == 0)
                {

                        string pathe = Path.Combine(Server.MapPath("~/Images/"), Path.GetFileName(file.FileName));
                        file.SaveAs(pathe);
                    
                    Ev.PhotoPath = "/Images/" +file.FileName;
                    Ev.EventTypeId = 2;
                    Ev.RegionId = 1;
                    db.Events.Add(Ev);
                    db.SaveChanges();
                    TempData["error"] = "تم حفظ المعلم بنجاح";
                }
                else
                {

                        string pathe = Path.Combine(Server.MapPath("~/Images/"), Path.GetFileName(file.FileName));
                        file.SaveAs(pathe);

                    


                    Ev.PhotoPath = "/Images/" + file.FileName;
                    Ev.Id =(int)EvId;
                    Ev.EventTypeId = 2;
                    Ev.RegionId = 1;
                    db.Entry(Ev).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["error"] = "تم التعديل بنجاح";
                }
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }


            return RedirectToAction("Details", "Event", new { id = Ev.Id });
        }
        public int Delete(int id)
        {
            try
            {
                var Ev = db.Events.Find(id);
                if (Ev != null)
                {
                    db.Events.Remove(Ev);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }

        [EncryptedActionParameter]

        public ActionResult Details(int Id)
        {
            if (Id == 0)
                return RedirectToAction("Index");
            var Ev = db.Events.FirstOrDefault(c => c.Id == Id);
            if (Ev == null)
                return RedirectToAction("Index");
            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);
            ViewBag.Regions = new SelectList(db.Regions.ToList(), "ID", "Name");
            ViewBag.Eventtype = new SelectList(db.EventTypes.ToList(), "Id", "Name");
            ViewBag.Cards = new SelectList(db.Cards.ToList(), "ClientID", "Name");


            return View(Ev);
        }


        //public ActionResult ChangeLocation(int? id, string lat, string lang)
        //{
        //    var lan = db.Landmarks.Find(id);
        //    lan.LongX = lat;
        //    lan.LatX = lang;
        //    db.Landmarks.AddOrUpdate(lan);
        //    db.SaveChanges();
        //    return RedirectToAction("Details", "LandMarks", new { id = id });
        //}
    }
}