﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SaveIt.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SaveIt.Entity;

namespace SaveIt.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
       // private ArabicGroupEntities db=new ArabicGroupEntities();
        public AdminController()
        {

        }
        private ApplicationUserManager _userManager;

        public AdminController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
  private SaveItEntities db = new SaveItEntities();

        public ActionResult Index()
        {
            var visitor = db.Visitors.ToList();
            return View();
        }
        [HttpGet]
        public ActionResult AddAdmin()
        {
            var users = UserManager.Users.Where(c=>c.Type==1).ToList();
            return View(users);
        }
        [HttpPost]
        public ActionResult AddAdmin(RegisterViewModel register)
        {
            var usr = UserManager.FindByName(register.UserName);
            if (usr == null)
            {
               // int userid = MAx.UserID(UserManager);
              var uu=  UserManager.Create(new ApplicationUser
                {
                    UserName = register.UserName,
                    FullName = register.FullName,
                    Email = register.UserName + "@Admin.com",
                    Type = 1,
                    UserID = MAx.UserID(UserManager)
                }, register.Password);
                if (uu.Succeeded)
                {
                    var us = UserManager.FindByName(register.UserName);
                    UserManager.AddToRole(us.Id, "Admin");
                }
            }
            return RedirectToAction("AddAdmin");
        }

        public ActionResult ResetRole(string UserName)
        {
            var us = UserManager.FindByName(UserName);
            UserManager.AddToRole(us.Id, "Admin");
            return RedirectToAction("AddAdmin");
        }

        public ActionResult DeleteRole(string UserName)
        {
            var us = UserManager.FindByName(UserName);
            UserManager.RemoveFromRole(us.Id, "Admin");
            return RedirectToAction("AddAdmin");  
        }

        public ActionResult DeleteAdmin(string UserName)
        {

            var us = UserManager.FindByName(UserName);

            if (UserManager.GetRoles(us.Id).Any())
            {
                var usrRoles = UserManager.GetRoles(us.Id);
                UserManager.RemoveFromRoles(us.Id, usrRoles.ToArray());
            }
            UserManager.Delete(us);
            return RedirectToAction("AddAdmin");    
        }
         [HttpPost]
        public ActionResult ResetUserName(string oldUserName, string newUserName)
        {
            var us = UserManager.FindByName(oldUserName);
            us.Email = newUserName+"@Admin.com";
            us.UserName = newUserName;
           var res= UserManager.Update(us);
           
            return RedirectToAction("AddAdmin");  
        }
         [HttpPost]
         public ActionResult ResetPassword(string PUserName, string NewPassword)
        {
            var usr = UserManager.FindByName(PUserName);
            if (usr != null)
            {
                string resetToken = UserManager.GeneratePasswordResetToken(usr.Id);
                IdentityResult passwordChangeResult = UserManager.ResetPassword(usr.Id, resetToken, NewPassword);
            }
            return RedirectToAction("AddAdmin");  
        }
	}
}