﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SaveIt.Entity;

namespace SaveIt.Areas.Admin.Controllers
{
     //[Authorize(Roles = "Admin")]
    public class CardsWebSiteController : Controller
    {
        private SaveItEntities db = new SaveItEntities();
        public ActionResult Index()
        {

            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);

            var wsCardss = db.Ws_Cards.Where(c=>c.Main==false).ToList();
            return View(wsCardss);
        }
        [HttpPost]
        public ActionResult Edit(Ws_Cards wsCards)
        {
            try
            {
                wsCards.Main = false;
                if (wsCards.ID == 0)
                {

                    db.Ws_Cards.Add(wsCards);
                    db.SaveChanges();
                    TempData["error"] = "تم حفظ  بنجاح";
                }
                else
                {
                    db.Ws_Cards.AddOrUpdate(wsCards);
                    db.SaveChanges();
                    TempData["error"] = "تم التعديل بنجاح";
                }
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }


            return RedirectToAction("Index");
        }
        public int Delete(int id)
        {
            try
            {
                var wsCards = db.Ws_Cards.Find(id);
                if (wsCards != null)
                {
                    db.Ws_Cards.Remove(wsCards);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }
	}
}