﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SaveIt.Entity;

namespace SaveIt.Areas.Admin.Controllers
{
     [Authorize(Roles = "Admin")]
    public class AcceptDonatorsController : Controller
    {
        private SaveItEntities db = new SaveItEntities();
        public ActionResult Index()
        {

            var donators = db.Donators.Where(c=>c.IsActive==0).ToList();
            return View(donators);
        }

   

        public int Accept(int id,int va)
        {
            try
            {
                var donator = db.Donators.Find(id);
                if (donator != null)
                {
                    donator.IsActive = va;
                    db.Entry(donator).State = EntityState.Modified;
                    db.SaveChanges();
              
                    return 1;
                  
                }
            }
            catch (Exception) { }

            return 0;
        }
    }
}