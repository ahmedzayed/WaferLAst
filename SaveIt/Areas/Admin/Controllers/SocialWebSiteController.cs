﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SaveIt.Entity;

namespace SaveIt.Areas.Admin.Controllers
{
     [Authorize(Roles = "Admin")]
    public class SocialWebSiteController : Controller
    {
        private SaveItEntities db = new SaveItEntities();
        public ActionResult Index()
        {

            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);

            var socialMedias = db.Ws_SocialMedia.ToList();
            return View(socialMedias);
        }
        [HttpPost]
        public ActionResult Edit(Ws_SocialMedia socialMedia)
        {
            try
            {
                if (socialMedia.ID == 0)
                {

                    db.Ws_SocialMedia.Add(socialMedia);
                    db.SaveChanges();
                    TempData["error"] = "تم حفظ  بنجاح";
                }
                else
                {
                    db.Ws_SocialMedia.AddOrUpdate(socialMedia);
                    db.SaveChanges();
                    TempData["error"] = "تم التعديل بنجاح";
                }
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }


            return RedirectToAction("Index");
        }
        public int Delete(int id)
        {
            try
            {
                var socialMedia = db.Ws_SocialMedia.Find(id);
                if (socialMedia != null)
                {
                    db.Ws_SocialMedia.Remove(socialMedia);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }
	}
}