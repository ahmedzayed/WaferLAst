﻿using SaveIt.Entity;
using SaveIt.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SaveIt.Areas.Admin.Controllers
{
    public class EventSponsorController : Controller
    {
        private SaveItEntities db = new SaveItEntities();
        public ActionResult Index()
        {

            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);

            var Evsp = db.EventSponsors.ToList();
            ViewBag.EventId= new SelectList(db.Events.ToList(), "Id", "Name");
            return View(Evsp);
        }
        [EncryptedActionParameter]

        public ActionResult GetSponsorById(int Id)
        {
            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);
            ViewBag.EventId = Id;
            var EvPh = db.EventSponsors.Where(a => a.EventId == Id).ToList();
            return View(EvPh);
        }
        [HttpPost]
        public ActionResult Edit(EventSponsor Evsp, HttpPostedFileBase file, int? EvSpId)
        {

            //try
            //{
            //    try
            //    {
                    if (EvSpId == 0)
                    {

                        string pathe = Path.Combine(Server.MapPath("~/Images/EventSponsor"), Path.GetFileName(file.FileName));
                        file.SaveAs(pathe);



                        Evsp.PhotoPath = "/Images/EventSponsor/"+file.FileName;

                        db.EventSponsors.Add(Evsp);
                        db.SaveChanges();
                        TempData["error"] = "تم حفظ التنظيم بنجاح";
                    }
                    else
                    {

                        string pathe = Path.Combine(Server.MapPath("~/Images/EventSponsor"), Path.GetFileName(file.FileName));
                        file.SaveAs(pathe);



                        Evsp.PhotoPath = "/Images/EventSponsor/"+file.FileName;
                Evsp.Id =(int) EvSpId;
                
                        db.Entry(Evsp).State = EntityState.Modified;
                        db.SaveChanges();
                        TempData["error"] = "تم التعديل بنجاح";
                    }
            //    }
            //    catch (Exception)
            //    {
            //        TempData["error"] = "يرجي ملئ كل البيانات";
            //    }
            //}
            //catch { }

                return RedirectToAction("Details", "Event", new { id = Evsp.EventId });
            
            }
        public int Delete(int id)
        {
            try
            {
                var Evsp = db.EventSponsors.Find(id);
                if (Evsp != null)
                {
                    db.EventSponsors.Remove(Evsp);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }
    }
}