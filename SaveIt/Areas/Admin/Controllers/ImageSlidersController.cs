﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SaveIt.Entity;

namespace SaveIt.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ImageSlidersController : Controller
    {
        private SaveItEntities db = new SaveItEntities();
        public ActionResult Index()
        {

            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);

            var Sliders = db.Ws_Slider.ToList();
            return View(Sliders);
        }
        [HttpPost]
        public ActionResult Edit(Ws_Slider Sliders, HttpPostedFileBase Photo)
        {
            try
            {
                if (Photo != null)
                {
                    try
                    {
                        if (Sliders.PhotoPath != null)
                        {
                            try
                            {
                                string filePath = Server.MapPath(Url.Content("~/" + Sliders.PhotoPath));
                                System.IO.File.Delete(filePath);
                            }
                            catch (Exception) { }
                        }
                        string fileName = Guid.NewGuid() + Path.GetExtension(Photo.FileName);
                        Photo.SaveAs(Path.Combine(Server.MapPath("~/Images"), fileName));
                        Sliders.PhotoPath = "/Images/" + fileName;
                    }
                    catch (Exception) { }
                }
                if (Sliders.PhotoPath != null)
                    if (Sliders.ID == 0)
                    {

                        db.Ws_Slider.Add(Sliders);
                        db.SaveChanges();
                        TempData["error"] = "تم حفظ  بنجاح";
                    }
                    else
                    {
                        db.Ws_Slider.AddOrUpdate(Sliders);
                        db.SaveChanges();
                        TempData["error"] = "تم التعديل بنجاح";
                    }
                else
                    TempData["error"] = "يرجي اختيار صورة";
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }


            return RedirectToAction("Index");
        }
        public int Delete(int id)
        {
            try
            {
                var Sliders = db.Ws_Slider.Find(id);
                if (Sliders != null)
                {
                    db.Ws_Slider.Remove(Sliders);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }
    }
}