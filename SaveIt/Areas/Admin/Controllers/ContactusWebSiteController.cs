﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SaveIt.Entity;

namespace SaveIt.Areas.Admin.Controllers
{
     [Authorize(Roles = "Admin")]
    public class ContactusWebSiteController : Controller
    {
        private SaveItEntities db = new SaveItEntities();
        public ActionResult Index()
        {

            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);

            var contactuss = db.Ws_Contactus.ToList();
            return View(contactuss);
        }

        public int Delete(int id)
        {
            try
            {
                var contactus = db.Ws_Contactus.Find(id);
                if (contactus != null)
                {
                    db.Ws_Contactus.Remove(contactus);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }
	}
}