﻿using SaveIt.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SaveIt.Areas.Admin
{
    public class OldCardController : Controller
    {
        // GET: Admin/OldCard
        private SaveItEntities db = new SaveItEntities();
        public ActionResult Index()
        {

            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);

            var News = db.OldCards.ToList();
            return View(News);
        }

         public ActionResult Create()
        {
            OldCard model = new OldCard();
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(OldCard oldCard)
        {

            try
            {
                if (oldCard.CardStartDate == null)
                {
                    oldCard.CardStartDate = DateTime.Now;
                }
                if (oldCard.CreatedDate == null)
                {
                    oldCard.CardStartDate = DateTime.Now;
                }

                if (oldCard.Id == 0)
                {
                    db.OldCards.Add(oldCard);
                    db.SaveChanges();
                    TempData["error"] = "تم حفظ البطاقة بنجاح";
                }
                else
                {
                    db.Entry(oldCard).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["error"] = "تم التعديل بنجاح";
                }
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }


            return RedirectToAction("Index");
        }
        public ActionResult Delete(int id)
        {
            try
            {
                var news = db.OldCards.Find(id);
                if (news != null)
                {
                    db.OldCards.Remove(news);
                    db.SaveChanges();
                    TempData["error"] = "تم حذف البطاقة بنجاح";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception) { }

            return RedirectToAction("Index");
        }

    }
}