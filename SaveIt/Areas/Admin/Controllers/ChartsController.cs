﻿using SaveIt.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SaveIt.Areas.Admin.Controllers
{
    public class ChartsController : Controller
    {
        private SaveItEntities db = new SaveItEntities();
        public ActionResult Index()
        {
            var register = db.ClientRegisterations.ToList();
            //var date = new DateTime(2017, 01, 01);
            var Enddate = new DateTime(DateTime.Now.Year + 1, 01, 01);
            var stat = "dataPoints: [";
            for (var date = new DateTime(2017, 01, 01); date < Enddate; date=date.AddMonths(1))
            {
                if (register.Any(c => c.DateCreate.Value.Month == date.Month && c.DateCreate.Value.Year == date.Year))
                {
                    var reg2 = register.Where(c => c.DateCreate.Value.Month == date.Month && c.DateCreate.Value.Year == date.Year);
                    var sum = reg2.Sum(c => c.Paid);
                    stat += "{ x: new Date("+date.Year+", "+date.Month+", "+date.Day+"), y: "+sum+", label: \""+date.Month+"-"+date.Year+"\" },";
                }
            }
            stat= stat.Substring(0, stat.Length - 1);
            stat += "]";
            ViewBag.Stats = stat;
            return View();
        }
        public ActionResult Count()
        {
            var register = db.ClientRegisterations.ToList();
            //var date = new DateTime(2017, 01, 01);
            var Enddate = new DateTime(DateTime.Now.Year + 1, 01, 01);
            var stat = "dataPoints: [";
            for (var date = new DateTime(2017, 01, 01); date < Enddate; date = date.AddMonths(1))
            {
                if (register.Any(c => c.DateCreate.Value.Month == date.Month && c.DateCreate.Value.Year == date.Year))
                {
                    var reg2 = register.Where(c => c.DateCreate.Value.Month == date.Month && c.DateCreate.Value.Year == date.Year);
                    var sum = reg2.Count();
                    stat += "{ x: new Date(" + date.Year + ", " + date.Month + ", " + date.Day + "), y: " + sum + ", label: \"" + date.Month + "-" + date.Year + "\" },";
                }
            }
            stat = stat.Substring(0, stat.Length - 1);
            stat += "]";
            ViewBag.Stats = stat;
            return View();
        }

        public ActionResult Visitors()
        {
            var register = db.Visitors.ToList();
            //var date = new DateTime(2017, 01, 01);
            long visitors = 0;
            var Enddate = new DateTime(DateTime.Now.Year+1, 01, 01);
            var stat = "dataPoints: [";
            for (var date = new DateTime(2018, 03, 01); date < Enddate; date = date.AddMonths(1))
            {
                if (register.Any(c => c.Month == date.Month && c.Year == date.Year))
                {
                    var reg2 = register.Where(c => c.Month == date.Month && c.Year == date.Year);
                    var sum = reg2.Sum(c=>c.Count)??0;
                    visitors += sum;
                    stat += "{ x: new Date(" + date.Year + ", " + date.Month + ", " + date.Day + "), y: " + sum + ", label: \"" + date.Month + "-" + date.Year + "\" },";
                }
            }
            stat = stat.Substring(0, stat.Length - 1);
            stat += "]";
            ViewBag.Stats = stat;
            ViewBag.visitors = visitors;
            return View();
        }


    }
}