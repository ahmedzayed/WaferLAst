﻿using SaveIt.Entity;
using SaveIt.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SaveIt.Areas.Admin.Controllers
{
    public class EventPhotoController : Controller
    {
        private SaveItEntities db = new SaveItEntities();
        public ActionResult Index()
        {

            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);

            var EvPh = db.EventPhotoes.ToList();

            ViewBag.EventId = new SelectList(db.Events.ToList(), "Id", "Name");
            ViewBag.Cards = new SelectList(db.Cards.ToList(), "ClientID", "Name");
            return View(EvPh);
        }
        [EncryptedActionParameter]

        public ActionResult GetPhotoById(int Id)
        {
            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);
            ViewBag.Cards = new SelectList(db.Cards.ToList(), "ClientID", "Name");
            ViewBag.EventId = Id;
            var EvPh = db.EventPhotoes.Where(a => a.EventId == Id).ToList();
            return View(EvPh);
        }
        [HttpPost]
        public ActionResult Edit(EventPhoto EvPh, HttpPostedFileBase[] file, int? IdPhoto)
        {

            try
            {


                if (IdPhoto == 0)
                {

                    foreach (var item in file)
                    {
                        string pathe = Path.Combine(Server.MapPath("~/Images/EventPhoto"), Path.GetFileName(item.FileName));
                        item.SaveAs(pathe);
                        EvPh.PhotoPath = "/Images/EventPhoto/"+item.FileName;
                        db.EventPhotoes.Add(EvPh);
                        db.SaveChanges();
                    }


                    TempData["error"] = "تم حفظ المعلم بنجاح";
                }
                else
                {
                    foreach (var item in file)
                    {
                        string pathe = Path.Combine(Server.MapPath("~/Images/EventPhoto"), Path.GetFileName(item.FileName));
                        item.SaveAs(pathe);
                        EvPh.PhotoPath = "/Images/EventPhoto/"+item.FileName;
                        EvPh.Id = (int)IdPhoto;
                        db.EventPhotoes.Add(EvPh);
                        db.Entry(EvPh).State = EntityState.Modified;
                    }

                    db.SaveChanges();
                    TempData["error"] = "تم التعديل بنجاح";
                }
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }


            return RedirectToAction("Details", "Event", new { id = EvPh.EventId });
        }
        public int Delete(int id)
        {
            try
            {
                var Evp = db.EventPhotoes.Find(id);
                if (Evp != null)
                {
                    db.EventPhotoes.Remove(Evp);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }
    }
}