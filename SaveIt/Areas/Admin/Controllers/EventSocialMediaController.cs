﻿using SaveIt.Entity;
using SaveIt.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SaveIt.Areas.Admin.Controllers
{
    public class EventSocialMediaController : Controller
    {
        private SaveItEntities db = new SaveItEntities();
        public ActionResult Index()
        {

            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);

            var Evsoc = db.EventSocialMedias.ToList();
            ViewBag.EventId = new SelectList(db.Events.ToList(), "Id", "Name");
            return View(Evsoc);
        }

        [EncryptedActionParameter]

        public ActionResult GetSocialById(int Id)
        {
            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);
            ViewBag.Cards = new SelectList(db.Cards.ToList(), "ClientID", "Name");
            ViewBag.EventId = Id;
            var EvPh = db.EventSocialMedias.Where(a => a.EventId == Id).ToList();
            return View(EvPh);
        }
        [HttpPost]
        public ActionResult Edit(EventSocialMedia Evsoc, int? EvSoId)
        {

            try
            {
                try
                {
                    if (EvSoId == 0)
                    {

                      

                        db.EventSocialMedias.Add(Evsoc);
                        db.SaveChanges();
                        TempData["error"] = "تم حفظ المعلم بنجاح";
                    }
                    else
                    {

                        Evsoc.Id = (int)EvSoId;
                        db.Entry(Evsoc).State = EntityState.Modified;
                        
                        db.SaveChanges();
                        TempData["error"] = "تم التعديل بنجاح";
                    }
                }
                catch (Exception)
                {
                    TempData["error"] = "يرجي ملئ كل البيانات";
                }
            }
            catch { }

            return RedirectToAction("Details", "Event", new { id = Evsoc.EventId });

        }
        public int Delete(int id)
        {
            try
            {
                var Evsoc = db.EventSocialMedias.Find(id);
                if (Evsoc != null)
                {
                    db.EventSocialMedias.Remove(Evsoc);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }
    }
}