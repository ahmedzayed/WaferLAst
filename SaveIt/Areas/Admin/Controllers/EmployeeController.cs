﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SaveIt.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SaveIt.Entity;

namespace SaveIt.Areas.Admin.Controllers
{
   [Authorize(Roles = "Admin")]
    public class EmployeeController : Controller
    {
       // private ArabicGroupEntities db=new ArabicGroupEntities();
        public EmployeeController()
        {

        }
        private ApplicationUserManager _userManager;

        public EmployeeController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Index()
        {

            return RedirectToAction("AddAdmin");
        }
        private SaveItEntities db = new SaveItEntities();
        [HttpGet]
        public ActionResult Statistics()
        {
            var users = UserManager.Users.Where(c => c.Type == 2).ToList();
            ViewBag. register = db.ClientRegisterations.ToList();
            return View(users);
        }

        public ActionResult Details(int id)
        {
            var register = db.ClientRegisterations.Where(c=>c.Card.UserID==id).GroupBy(c=>c.Card);
           ViewBag.fullname = db.AspNetUsers.FirstOrDefault(c => c.UserID == id).FullName;
            return View(register);
        }
        
        [HttpGet]
        public ActionResult AddAdmin()
        {
            var users = UserManager.Users.Where(c=>c.Type==2).ToList();
            return View(users);
        }
        [HttpPost]
        public ActionResult AddAdmin(RegisterViewModel register)
        {
            var usr = UserManager.FindByName(register.UserName);
            if (usr == null)
            {
               // int userid = MAx.UserID(UserManager);
              var uu=  UserManager.Create(new ApplicationUser
                {
                    UserName = register.UserName,
                    FullName = register.FullName,
                    Email = register.UserName + "@Employee.com",
                    Type = 2,
                  UserID = MAx.UserID(UserManager)
              }, register.Password);
                if (uu.Succeeded)
                {
                    var us = UserManager.FindByName(register.UserName);
                    UserManager.AddToRole(us.Id, "Employee");
                }
            }
            return RedirectToAction("AddAdmin");
        }

        public ActionResult ResetRole(string UserName)
        {
            var us = UserManager.FindByName(UserName);
            UserManager.AddToRole(us.Id, "Employee");
            return RedirectToAction("AddAdmin");
        }

        public ActionResult DeleteRole(string UserName)
        {
            var us = UserManager.FindByName(UserName);
            UserManager.RemoveFromRole(us.Id, "Employee");
            return RedirectToAction("AddAdmin");  
        }

        public ActionResult DeleteAdmin(string UserName)
        {

            var us = UserManager.FindByName(UserName);

            if (UserManager.GetRoles(us.Id).Any())
            {
                var usrRoles = UserManager.GetRoles(us.Id);
                UserManager.RemoveFromRoles(us.Id, usrRoles.ToArray());
            }
            UserManager.Delete(us);
            return RedirectToAction("AddAdmin");    
        }
         [HttpPost]
        public ActionResult ResetUserName(string oldUserName, string newUserName)
        {
            var us = UserManager.FindByName(oldUserName);
            us.Email = newUserName+"@Employee.com";
            us.UserName = newUserName;
           var res= UserManager.Update(us);
           
            return RedirectToAction("AddAdmin");  
        }
         [HttpPost]
         public ActionResult ResetPassword(string PUserName, string NewPassword)
        {
            var usr = UserManager.FindByName(PUserName);
            if (usr != null)
            {
                string resetToken = UserManager.GeneratePasswordResetToken(usr.Id);
                IdentityResult passwordChangeResult = UserManager.ResetPassword(usr.Id, resetToken, NewPassword);
            }
            return RedirectToAction("AddAdmin");  
        }
	}
}