﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SaveIt.Entity;

namespace SaveIt.Areas.Admin.Controllers
{
     [Authorize(Roles = "Admin")]
    public class WebsiteStatisticsController : Controller
    {
        private SaveItEntities db = new SaveItEntities();
        public ActionResult Index()
        {

            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);

            var statisticss = db.WebsiteStatistics.ToList();
            return View(statisticss);
        }
        [HttpPost]
        public ActionResult Edit(WebsiteStatistic statistics)
        {
            try
            {
                if (statistics.Id == 0)
                {

                    db.WebsiteStatistics.Add(statistics);
                    db.SaveChanges();
                    TempData["error"] = "تم حفظ  بنجاح";
                }
                else
                {
                    db.WebsiteStatistics.AddOrUpdate(statistics);
                    db.SaveChanges();
                    TempData["error"] = "تم التعديل بنجاح";
                }
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }


            return RedirectToAction("Index");
        }
        public int Delete(int id)
        {
            try
            {
                var statistics = db.WebsiteStatistics.Find(id);
                if (statistics != null)
                {
                    db.WebsiteStatistics.Remove(statistics);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }
	}
}