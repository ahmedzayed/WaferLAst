﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SaveIt.Entity;

namespace SaveIt.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AcceptImagesController : Controller
    {
        private SaveItEntities db = new SaveItEntities();
        public ActionResult Index()
        {
            var photoss = db.Donators.Where(c=>c.DonatorImages.Any(cx=>cx.IsActive==0)).ToList();
            return View(photoss);
        }
        public int Edit(int? id,int? type)
        {
            //try
            //{
                if (type != null)
                {
                    var donatImage = db.DonatorImages.Find(id);
                    donatImage.IsActive = (int)type;
                    db.DonatorImages.AddOrUpdate(donatImage);
                    db.SaveChanges();
                    return (int)type;
                }
            //}
            //catch (Exception) { }
            return 0;
        }

    }
}