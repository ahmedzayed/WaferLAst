﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SaveIt.Entity;
using System.Data.Entity.Migrations;

namespace SaveIt.Areas.Admin.Controllers
{
     [Authorize(Roles = "Admin")]
    public class EvaluationQuestionsController : Controller
    {
        private SaveItEntities db = new SaveItEntities();
        public ActionResult Index()
        {
          
            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);

            var EvaluationQuestions = db.EvaluationQuestions.Where(c=>c.IsDelete!=true).ToList();
            return View(EvaluationQuestions);
        }
        [HttpPost]
        public ActionResult Edit(EvaluationQuestion evaluationQuestion)
        {
            try
            {
                if (evaluationQuestion.Id == 0)
                {

                    db.EvaluationQuestions.Add(evaluationQuestion);
                    db.SaveChanges();
                    TempData["error"] = "تم حفظ التخصص بنجاح";
                }
                else
                {
                    db.Entry(evaluationQuestion).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["error"] = "تم التعديل بنجاح";
                }
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }


            return RedirectToAction("Index");
        }
        public int Delete(int id)
        {
            try
            {
                var evaluationQuestion = db.EvaluationQuestions.Find(id);
                if (evaluationQuestion != null)
                {
                    evaluationQuestion.IsDelete = true;
                    db.EvaluationQuestions.AddOrUpdate(evaluationQuestion);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }
    }
}