﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SaveIt.Entity;

namespace SaveIt.Areas.Admin.Controllers
{
     [Authorize(Roles = "Admin")]
    public class WebsiteContactInfoController : Controller
    {
        private SaveItEntities db = new SaveItEntities();
        public ActionResult Index()
        {

            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);

            var infos = db.WebSiteContactInfoes.ToList();
            return View(infos);
        }
        [HttpPost]
        public ActionResult Edit(WebSiteContactInfo info)
        {
            try
            {
                if (info.Id == 0)
                {

                    db.WebSiteContactInfoes.Add(info);
                    db.SaveChanges();
                    TempData["error"] = "تم حفظ  بنجاح";
                }
                else
                {
                    db.WebSiteContactInfoes.AddOrUpdate(info);
                    db.SaveChanges();
                    TempData["error"] = "تم التعديل بنجاح";
                }
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }


            return RedirectToAction("Index");
        }
        public int Delete(int id)
        {
            try
            {
                var info = db.WebSiteContactInfoes.Find(id);
                if (info != null)
                {
                    db.WebSiteContactInfoes.Remove(info);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }
	}
}