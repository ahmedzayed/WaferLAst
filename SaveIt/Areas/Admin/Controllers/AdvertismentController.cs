﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SaveIt.Entity;

namespace SaveIt.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdvertismentController : Controller
    {
        private SaveItEntities db = new SaveItEntities();
        public ActionResult Index()
        {

            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);

            var photoss = db.Advertisments.ToList();
            return View(photoss);
        }
        [HttpPost]
        public ActionResult Edit(Advertisment ads, HttpPostedFileBase Photo)
        {
            try
            {
                if (Photo != null)
                {
                    try
                    {
                        if (ads.PhotoPath != null)
                        {
                            try
                            {
                                string filePath = Server.MapPath(Url.Content("~/" + ads.PhotoPath));
                                System.IO.File.Delete(filePath);
                            }
                            catch (Exception) { }
                        }
                        string fileName = Guid.NewGuid() + Path.GetExtension(Photo.FileName);
                        Photo.SaveAs(Path.Combine(Server.MapPath("~/Images"), fileName));
                        ads.PhotoPath = "/Images/" + fileName;
                    }
                    catch (Exception) { }
                }

                var dd = (DateTime)ads.FinishDate - DateTime.Now;
                var ddd = dd.Days;
                if (ddd > 365)
                {
                    var ye = ddd / 365;
                    var days = ddd - ye * 365;
                    if (days > 30)
                    {
                        int mo = days / 30;
                        int day = days - mo * 30;
                        ads.Period = ye + " عام -" + mo + " شهر -" + day + "  يوم";
                    }
                }
                else if (ddd > 30)
                {
                    int mo = ddd / 30;
                    int day = ddd - mo * 30;
                    ads.Period = mo + " شهر -" + day + "  يوم";
                }
                else
                {
                    ads.Period = ddd + " يوم";
                }

                if (ads.PhotoPath != null)
                    if (ads.Id == 0)
                    {

                        db.Advertisments.Add(ads);
                        db.SaveChanges();
                        TempData["error"] = "تم حفظ  بنجاح";
                    }
                    else
                    {
                        db.Advertisments.AddOrUpdate(ads);
                        db.SaveChanges();
                        TempData["error"] = "تم التعديل بنجاح";
                    }
                else
                    TempData["error"] = "يرجي اختيار صورة";
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }


            return RedirectToAction("Index");
        }
        public int Delete(int id)
        {
            try
            {
                var photos = db.Advertisments.Find(id);
                if (photos != null)
                {
                    db.Advertisments.Remove(photos);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }
    }
}