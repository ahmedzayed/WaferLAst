﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SaveIt.Entity;

namespace SaveIt.Areas.Admin.Controllers
{
     [Authorize(Roles = "Admin")]
    public class RegionController : Controller
    {
        private SaveItEntities db = new SaveItEntities();
        public ActionResult Index()
        {
          
            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);

            var Regions = db.Regions.ToList();
            return View(Regions);
        }
        [HttpPost]
        public ActionResult Edit(Region Region)
        {
            try
            {
                if (Region.ID == 0)
                {

                    db.Regions.Add(Region);
                    db.SaveChanges();
                    TempData["error"] = "تم حفظ التخصص بنجاح";
                }
                else
                {
                    db.Entry(Region).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["error"] = "تم التعديل بنجاح";
                }
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }


            return RedirectToAction("Index");
        }
        public int Delete(int id)
        {
            try
            {
                var Region = db.Regions.Find(id);
                if (Region != null)
                {
                    db.Regions.Remove(Region);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }
    }
}