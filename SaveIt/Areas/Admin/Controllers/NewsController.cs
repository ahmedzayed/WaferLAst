﻿using SaveIt.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SaveIt.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class NewsController : Controller
    {
        // GET: Admin/News
        private SaveItEntities db = new SaveItEntities();
        public ActionResult Index()
        {

            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);

            var News = db.News.ToList();
            return View(News);
        }
        [HttpPost]
        public ActionResult Edit(News news, HttpPostedFileBase file)
        {

            try
            {
                try
                {
                    
                    if (file != null)
                    {
                        string fileName = Guid.NewGuid() + Path.GetExtension(file.FileName);
                        file.SaveAs(Path.Combine(Server.MapPath("~/Images"), fileName));
                        news.PhotoPath = "/Images/" + fileName;
                    }
 
                    news.Date = DateTime.Now;

                }
                catch (Exception) { }

                if (news.Id == 0)
                {
                    db.News.Add(news);
                    db.SaveChanges();
                    TempData["error"] = "تم حفظ الخبر بنجاح";
                }
                else
                {
                    db.Entry(news).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["error"] = "تم التعديل بنجاح";
                }
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }


            return RedirectToAction("Index");
        }
        public int Delete(int id)
        {
            try
            {
                var news = db.News.Find(id);
                if (news != null)
                {
                    db.News.Remove(news);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }
    }
}