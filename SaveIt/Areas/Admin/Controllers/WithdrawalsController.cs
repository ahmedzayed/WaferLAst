﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SaveIt.Entity;

namespace SaveIt.Areas.Admin.Controllers
{
     [Authorize(Roles = "Admin")]
    public class WithdrawalsController : Controller
    {
        private SaveItEntities db = new SaveItEntities();
        public ActionResult Index()
        {

            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);

            var withdrawals = db.withdrawals.ToList();
            return View(withdrawals);
        }

        public int Delete(int id)
        {
            try
            {
                var contactus = db.withdrawals.Find(id);
                if (contactus != null)
                {
                    db.withdrawals.Remove(contactus);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }
	}
}