﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaveIt.Areas.Donators.Models
{
    public class Discounts
    {
        public int DiscountPercentage { get; set; }
        public int Discountvalue { get; set; }
        public int Paid { get; set; }
        public int TempDiscount { get; set; }
    }
}