﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SaveIt.Entity;
using SaveIt.Models;

namespace SaveIt.Areas.Donators.Controllers
{
    [Authorize(Roles = "Donator")]
    public class ImagesController : Controller
    {
              private SaveItEntities db = new SaveItEntities();

          public ImagesController()
        {

        }
        private ApplicationUserManager _userManager;

        public ImagesController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public ActionResult Index()
        {
            var usr = UserManager.FindById(User.Identity.GetUserId());
            var don = db.DonatorImages.Where(c=>c.DonatorID==usr.UserID);
            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);
            return View(don);
        }
        [HttpPost]
        public ActionResult Edit(DonatorImage photos, HttpPostedFileBase Photo)
        {

            try
            {
                if (Photo != null)
                {
                    try
                    {
                        if (photos.Image != null)
                        {
                            try
                            {
                                string filePath = Server.MapPath(Url.Content("~/" + photos.Image));
                                System.IO.File.Delete(filePath);
                            }
                            catch (Exception) { }
                        }
                        string fileName = Guid.NewGuid() + Path.GetExtension(Photo.FileName);
                        Photo.SaveAs(Path.Combine(Server.MapPath("~/Images"), fileName));
                        photos.Image = "/Images/" + fileName;
                    }
                    catch (Exception) { }
                }
                if (photos.Image != null)
                    if (photos.ID == 0)
                    {
                        photos.DonatorID = UserManager.FindById(User.Identity.GetUserId()).UserID;
                        db.DonatorImages.Add(photos);
                        db.SaveChanges();
                        TempData["error"] = "تم حفظ  بنجاح";
                    }
                    else
                    {
                     
                        var ims = db.DonatorImages.Find(photos.ID);
                        ims.Image = photos.Image;
                        db.DonatorImages.AddOrUpdate(ims);
                        db.SaveChanges();
                        TempData["error"] = "تم التعديل بنجاح";
                    }
                else
                    TempData["error"] = "يرجي اختيار صورة";
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }


            return RedirectToAction("Index");
        }
        public int Delete(int id)
        {
            try
            {
                var photos = db.DonatorImages.Find(id);
                if (photos != null)
                {
                    db.DonatorImages.Remove(photos);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }
    }
}