﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SaveIt.Entity;
using System.IO;

namespace SaveIt.Areas.Donators.Controllers
{
     [Authorize(Roles = "Admin")]
    public class CategoryController : Controller
    {
        private SaveItEntities db = new SaveItEntities();
        public ActionResult Index()
        {
          
            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);

            var Categorys = db.Categorys.ToList();
            return View(Categorys);
        }
        [HttpPost]
        public ActionResult Edit(Category Category,HttpPostedFileBase file)
        {
            
            try
            {
                try
                {
                if (file != null){
                    string fileName = Guid.NewGuid() + Path.GetExtension(file.FileName);
                    file.SaveAs(Path.Combine(Server.MapPath("~/Images"), fileName));
                    Category.Photo = "/Images/" + fileName;
                }

                }
                catch (Exception) { }
                
                if (Category.ID == 0)
                {
                    db.Categorys.Add(Category);
                    db.SaveChanges();
                    TempData["error"] = "تم حفظ التخصص بنجاح";
                }
                else
                {
                    db.Entry(Category).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["error"] = "تم التعديل بنجاح";
                }
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }


            return RedirectToAction("Index");
        }
        public int Delete(int id)
        {
            try
            {
                var Category = db.Categorys.Find(id);
                if (Category != null)
                {
                    db.Categorys.Remove(Category);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }
    }
}