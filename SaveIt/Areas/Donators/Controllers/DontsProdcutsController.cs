﻿using SaveIt.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SaveIt.Areas.Donators.Controllers
{
    //[Authorize(Roles = "Donator")]
    public class DontsProdcutsController : Controller
    {
        // GET: Admin/DontsProdcuts
        private SaveItEntities db = new SaveItEntities();

        public ActionResult Index()
        {
            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);
            var m = Convert.ToInt32(Session["DonatorID"]);
            var Crd = db.Products.Where(x => x.IsActive == true && x.DontorID == m).ToList();
            var mn = Crd.FirstOrDefault().DontorID;


            ViewBag.objDonators = new SelectList(db.ProductsCategories.ToList(), "ID", "Name");

            return View(Crd);
        }
        public JsonResult getprod(int Catid)
        {
            var objDonators = db.ProductsCategories.Where(x => x.DonatorID == Catid).ToList();

            return new JsonResult { Data = objDonators, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        [HttpPost]
        public ActionResult Edit(Product Prd, HttpPostedFileBase file)
        {
            try
            {

                try
                {

                    if (file != null)
                    {
                        string fileName = Guid.NewGuid() + Path.GetExtension(file.FileName);
                        file.SaveAs(Path.Combine(Server.MapPath("~/Images"), fileName));
                        Prd.Photo = "/Images/" + fileName;
                    }
                }
                catch (Exception) { }


                if (Prd.ID == 0)
                {
                    Prd.IsActive = true;
                    Prd.DontorID = Convert.ToInt32(Session["DonatorID"]);
                    Prd.IsActive = true;

                    db.Products.Add(Prd);
                    db.SaveChanges();
                    TempData["error"] = "تم حفظ التصنيف بنجاح";
                }
                else
                {
                    db.Entry(Prd).State = EntityState.Modified;
                    Prd.IsActive = true;
                    Prd.DontorID = Convert.ToInt32(Session["DonatorID"]);
                    db.SaveChanges();
                    TempData["error"] = "تم التعديل بنجاح";
                }
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }


            return RedirectToAction("Index");
        }

        public int Delete(int id)
        {
            var PrdCat = db.Products.Find(id);
            if (PrdCat != null)
            {
                PrdCat.IsActive = false;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}