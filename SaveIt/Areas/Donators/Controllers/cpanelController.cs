﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SaveIt.Entity;
using SaveIt.Models;

namespace SaveIt.Areas.Donators.Controllers
{
    [Authorize(Roles = "Donator")]
    public class cpanelController : Controller
    {
              private SaveItEntities db = new SaveItEntities();

          public cpanelController()
        {

        }
        private ApplicationUserManager _userManager;

        public cpanelController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public ActionResult Index()
        {
            var usr = UserManager.FindById(User.Identity.GetUserId());
            var don = db.Donators.Find(usr.UserID);
            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);
            ViewBag.CatID = new SelectList(db.Categorys, "ID", "Name", don.CatID);
            return View("Details",don);
        }


        [HttpPost]
        public ActionResult Edit(Donator donator)
        {
            try
            {
                if (donator.ID == 0)
                {
                    donator.ContractDate = DateTime.Now;
                    donator.IsActive = 0;
                    db.Donators.Add(donator);
                    db.SaveChanges();
                    TempData["error"] = "تم حفظ المانح بنجاح";
                }
                else
                {
                    db.Entry(donator).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["error"] = "تم التعديل بنجاح";
                }
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }

            return RedirectToAction("Index", new { id = donator.ID });

        }

        [HttpPost]
        public ActionResult SetSocialMedia(DonatorContact contact)
        {

            try
            {
                db.DonatorContacts.AddOrUpdate(contact);
                db.SaveChanges();
                TempData["error"] = "تم الاضافة بنجاح";
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ادخال البيانات صحيحة";
            }
            return RedirectToAction("Details", new { id = contact.DonatorID });
        }
        public void DeleteSocial(int id)
        {
            var con = db.DonatorContacts.Find(id);
            int? ids = con.DonatorID;
            db.DonatorContacts.Remove(con);
            db.SaveChanges();
        }

        [HttpPost]
        public ActionResult ResetUserName(int DonatorID, string oldUserName, string newUserName)
        {
            var us = UserManager.FindByName(oldUserName);
            if (us != null)
            {
                us.Email = newUserName + "@Donator.com";
                us.UserName = newUserName;
                var res = UserManager.Update(us);
                if (res.Succeeded)
                {
                    var donat = db.Donators.Find(DonatorID);
                    donat.Username = newUserName;
                    db.Donators.AddOrUpdate(donat);
                    db.SaveChanges();
                    TempData["error"] = "تم تغيير اسم المستخدم بنجاح";
                }
                else
                {
                    TempData["error"] = "يرجي ادخال اسم مستخدم اخر";
                }
            }
            else
            {
                TempData["error"] = "الحساب غير موجود";
            }


            return RedirectToAction("Index", new { id = DonatorID });
        }
        [HttpPost]
        public ActionResult ResetPassword(int DonatorID, RegisterViewModel reg)
        {
            if (!reg.Password.Equals(reg.ConfirmPassword))
            {
                TempData["error"] = "كلمة المرور غير مطابقة للتاكيد";
                return RedirectToAction("Index", new { id = DonatorID });
            }

            var usr = UserManager.FindByName(reg.UserName);
            if (usr != null)
            {
                string resetToken = UserManager.GeneratePasswordResetToken(usr.Id);
                IdentityResult result = UserManager.ResetPassword(usr.Id, resetToken, reg.Password);
                if (result.Succeeded)
                    TempData["error"] = "تم تغيير كلمة المرور بنجاح";
                else
                    TempData["error"] = " لم يتم تغيير كلمة المرور ";

            }
            else
            {
                TempData["error"] = "الحساب غير موجود";
            }

            return RedirectToAction("Index", new { id = DonatorID });
        }

        [HttpPost]
        public ActionResult DonatService(DonatService serv)
        {
            try
            {
                if (serv.ID == 0)
                {

                    db.DonatServices.Add(serv);
                    db.SaveChanges();
                    TempData["error"] = "تم حفظ  بنجاح";
                }
                else
                {
                    db.Entry(serv).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["error"] = "تم التعديل بنجاح";
                }
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }

            return RedirectToAction("Index", new { id = serv.DonatorID });
        }

        public int DeleteDonserv(int id)
        {
            try
            {
                var Services = db.DonatServices.Find(id);
                if (Services != null)
                {
                    db.DonatServices.Remove(Services);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }
        [HttpPost]
        public ActionResult DonatedService(DonatedService serv)
        {
            try
            {
                if (serv.ID == 0)
                {

                    db.DonatedServices.Add(serv);
                    db.SaveChanges();
                    TempData["error"] = "تم حفظ  بنجاح";
                }
                else
                {
                    db.Entry(serv).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["error"] = "تم التعديل بنجاح";
                }
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }

            return RedirectToAction("Index", new { id = serv.DonatorID });
        }

        public int DeleteDonedserv(int id)
        {
            try
            {
                var Services = db.DonatedServices.Find(id);
                if (Services != null)
                {
                    db.DonatedServices.Remove(Services);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }
          [HttpPost]
        public ActionResult updatePhoto(int PID, HttpPostedFileBase uploadfile)
        {
            if (uploadfile == null)
            {
                TempData["error"] = "من فضلك اضف صوره";
                return RedirectToAction("Index", new { id = PID });
            }

            try
            {
                var pg = db.Donators.Find(PID);
                if (pg.Photo != null)
                {
                    try
                    {
                        string filePath = Server.MapPath(Url.Content("~/" + pg.Photo));
                        System.IO.File.Delete(filePath);
                    }
                    catch (Exception)
                    {

                    }
                 
                }
                string fileName = Guid.NewGuid() + Path.GetExtension(uploadfile.FileName);
                uploadfile.SaveAs(Path.Combine(Server.MapPath("~/Images"), fileName));
                pg.Photo = "/Images/" + fileName;
                db.Donators.AddOrUpdate(pg);
                db.SaveChanges();
                TempData["error"] = "تم اضافة الصورة بنجاح";
            }
            catch
            {
                TempData["error"] = "من فضلك ان لايزيد الصورة عن 2 ميجا";
            }

            return RedirectToAction("Index", new { id = PID });
        }


	}
}