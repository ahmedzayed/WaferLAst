﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SaveIt.Entity;
using SaveIt.Models;
using SaveIt.ModelView;

namespace SaveIt.Areas.Donators.Controllers
{
    // [Authorize(Roles = "Admin")]
    public class ChatRoomsController : Controller
    {
        private readonly string _connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        private SaveItEntities db = new SaveItEntities();

        public ChatRoomsController()
        {

        }
        private ApplicationUserManager _userManager;

        public ChatRoomsController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public ActionResult Index()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            var demoned = db.DemondChats.Where(c => c.UserID == user.UserID).OrderByDescending(c => c.ID).ToList();

            return View(demoned);
        }

        public void SetDemond()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            //var demond=new DemondChat {UserID = user.UserID,UserType = "Donator",Status = 0,CreateDate = DateTime.Now};
            //db.DemondChats.AddOrUpdate(demond);
            //db.SaveChanges();
            using (var connection = new SqlConnection(_connString))
            {
                var date = DateTime.Now.ToString("MM/dd/yyyy hh:mm");
                connection.Open();
                string com = "INSERT INTO [dbo].[DemondChat]([UserType],[UserID],[Status],[CreateDate]) VALUES ('Donator'," + user.UserID + ",0,'" + date + "')";
                var commend = new SqlCommand(com, connection);
                commend.ExecuteNonQuery();
            }
            
          
        }
    }
}