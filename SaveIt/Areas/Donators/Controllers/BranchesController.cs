﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SaveIt.Entity;

namespace SaveIt.Areas.Donators.Controllers
{
    [Authorize(Roles = "Donator")]
    public class BranchesController : Controller
    {

        private SaveItEntities db = new SaveItEntities();

        public BranchesController()
        {

        }
        private ApplicationUserManager _userManager;

        public BranchesController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Index()
        {
            var usr = UserManager.FindById(User.Identity.GetUserId());
            var oders = db.DonatorBranches.Where(c => c.DonatorID == usr.UserID).ToList();
            return View(oders);
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            if(id==0)
                return PartialView(new DonatorBranch());
            var branch = db.DonatorBranches.Find(id);
            return PartialView(branch);
        }
        [HttpPost]
        public ActionResult Edit(DonatorBranch order)
        {
            try
            {
                var usr = UserManager.FindById(User.Identity.GetUserId());
                order.DonatorID = usr.UserID;
                // db.Entry(condetion).State = EntityState.Modified;
                db.DonatorBranches.AddOrUpdate(order);
                db.SaveChanges();
                TempData["error"] = "تم حفظ التغييرات بنجاح";

            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }
            return RedirectToAction("Index");
        }


        public int Delete(int id)
        {
            try
            {
                var order = db.DonatorBranches.Find(id);
                if (order != null)
                {
                    db.DonatorBranches.Remove(order);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }

    }
}