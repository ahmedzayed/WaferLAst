﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SaveIt.Entity;

namespace SaveIt.Areas.Donators.Controllers
{
    [Authorize(Roles = "Donator")]
    public class TempOrdersController : Controller
    {

        private SaveItEntities db = new SaveItEntities();

        public TempOrdersController()
        {

        }
        private ApplicationUserManager _userManager;

        public TempOrdersController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Index()
        {
            var usr = UserManager.FindById(User.Identity.GetUserId());
            ViewBag.ServiceId = new SelectList(db.DonatServices.Where(c=>c.DonatorID==usr.UserID).ToList(), "ID", "Service");
            var oders = db.TempDiscountServices.Where(c => c.DonatorId == usr.UserID).ToList();
            return View(oders);
        }


        [HttpPost]
        public ActionResult Edit(TempDiscountService order)
        {
            try
            {
                var usr = UserManager.FindById(User.Identity.GetUserId());
                order.DonatorId = usr.UserID;
                order.IsAccepted = null;
                order.IsOrder = true;
                db.TempDiscountServices.AddOrUpdate(order);
                db.SaveChanges();
                TempData["error"] = "تم حفظ التغييرات بنجاح";
            }
            catch (Exception)
            {
                TempData["error"] = "يرجي ملئ كل البيانات";
            }
            return RedirectToAction("Index");
        }


        public int Delete(int id)
        {
            try
            {
                var order = db.TempDiscountServices.Find(id);
                if (order != null)
                {
                    db.TempDiscountServices.Remove(order);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }

    }
}