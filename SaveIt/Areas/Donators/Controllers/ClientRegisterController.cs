﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SaveIt.Areas.Donators.Models;
using SaveIt.Entity;

namespace SaveIt.Areas.Donators.Controllers
{
    [Authorize(Roles = "Donator")]
    public class ClientRegisterController : Controller
    {

        private SaveItEntities db = new SaveItEntities();

        public ClientRegisterController()
        {

        }
        private ApplicationUserManager _userManager;

        public ClientRegisterController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Index()
        {
            var usr = UserManager.FindById(User.Identity.GetUserId());
            var reg = db.ClientRegisterations.Where(c => c.DonatorID == usr.UserID).ToList();
            return View(reg);
        }


        public ActionResult Getcarddetails(int id)
        {
            var card = db.Cards.Find(id);
            if (card == null)
            {
                throw new Exception("please enter card number correctly");
            }
            if (card.FinishDate < DateTime.Now)
            {
                return Content("0");
            }
            var usr = UserManager.FindById(User.Identity.GetUserId());
            ViewBag.ServiceID = new SelectList(db.DonatServices.Where(c => c.DonatorID == usr.UserID).ToList(), "ID", "Service");
            return PartialView(card);
        }

        public ActionResult getDiscount(int id,int tot)
        {
            var serv = db.DonatServices.Find(id);
            if (serv == null)
            {
                throw new Exception("please enter card number correctly");
            }
            var discount=new Discounts();
            discount.DiscountPercentage =serv.Discount==null?0: (int)serv.Discount;
            discount.Discountvalue = (discount.DiscountPercentage * tot) / 100;
            discount.Paid = tot - discount.Discountvalue;
            var time = new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            var date = DateTime.Now.Date;
            if (db.TempDiscountServices.Any(c => c.ServiceId == id&&c.TimeFrom<time&&c.TimeTo>time&&c.DateForm<date&&c.DateTo>date))
            {
                var desi = db.TempDiscountServices.FirstOrDefault(c => c.ServiceId == id && c.TimeFrom < time && c.TimeTo > time && c.DateForm < date && c.DateTo > date);
                discount.TempDiscount = (int)((desi.Discount* tot) / 100);
                discount.Paid = discount.Paid - (discount.TempDiscount );
            }

          return PartialView(discount);
        }


        [HttpPost]
        public ActionResult Edit(ClientRegisteration registeration)
        {
            try
            {
                var usr = UserManager.FindById(User.Identity.GetUserId());
                registeration.DonatorID = usr.UserID;
                registeration.DateCreate = DateTime.Now;
                db.ClientRegisterations.AddOrUpdate(registeration);
               db.SaveChanges();
                TempData["error"] = "تم حفظ التغييرات بنجاح";
                registeration.Card = db.Cards.Find(registeration.ClientID);
                registeration.DonatService = db.DonatServices.Find(registeration.ServiceID);
                return PartialView(registeration);
            }
            catch (Exception)
            {
              throw new Exception("please check your information");
            }
           
        }


        public int Delete(int id)
        {
            try
            {
                var reg = db.ClientRegisterations.Find(id);
                if (reg != null)
                {
                    db.ClientRegisterations.Remove(reg);
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception) { }

            return 0;
        }

    }
}