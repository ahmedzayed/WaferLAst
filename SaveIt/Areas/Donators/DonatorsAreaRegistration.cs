﻿using System.Web.Mvc;

namespace SaveIt.Areas.Donators
{
    public class DonatorsAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Donators";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Donators_default",
                "Donators/{controller}/{action}/{id}",
                new { controller = "cpanel", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}