﻿using SaveIt.WebApi.Enitity;
using SaveIt.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace SaveIt.WebApi.Controllers
{
    public class DepartmentController : ApiController
    {
      
        private waferEntities db = new waferEntities();
        public IHttpActionResult Get()
        {
            var areas = db.Categorys.Select(c=>new AreaModel {
                Id=c.ID,
                Name=c.Name,
                CountProvider=c.Donators.Count,
                Photo= c.Photo!=null? Basic.WebsiteUrl + c.Photo:""
            });
            return Ok(areas);
        }
    }
}
