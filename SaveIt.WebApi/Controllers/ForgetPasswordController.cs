﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using SaveIt.WebApi.Enitity;
using SaveIt.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SaveIt.WebApi.Controllers
{
    public class ForgetPasswordController : ApiController
    {
        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;
        public ForgetPasswordController()
        {
        }
        public ForgetPasswordController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }
        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        private waferEntities db = new waferEntities();
        public IHttpActionResult Get(string Email)
        {
            var model = new RegisterRequest();

            var card = db.Cards.FirstOrDefault(c => c.Email == Email);
            if (card == null)
            {
                model.Code = 0;
                model.RequstDetails = "هذا الايميل غير موجود ";
            }
            else
            {
                MailSender.SendMessage(Email, "يرجي ادخال هذا الكود في التطبيق لاعادة تعيين كلمة المرور",card.ClientID+"", "هل نسيت كلمة المرور");
                model.Code = 1;
                model.RequstDetails = "تم ارسال ايميل بالكود";
            }
            return Ok(model);
        }


        // POST: api/ResetPassword
        public IHttpActionResult Post([FromBody]ForgetModel forget)
        {   var model = new RegisterRequest();
            var user = UserManager.Users.FirstOrDefault(c => c.UserID == forget.Code);
            if (user == null)
            {
                model.Code = 0;
                model.RequstDetails = "حدث خطا الكود غير صحيح";
            }else
            {

                var token = UserManager.GeneratePasswordResetToken(user.Id);
                var res = UserManager.ResetPassword(user.Id, token, forget.Password);
                if (res.Succeeded)
                {
                    model.Code = 1;
                    model.RequstDetails = "تم تغيير الباسورد بنجاح";
                }
                else
                {
                    model.Code = 0;
                    model.RequstDetails = "حدث خطا في تغيير الباسورد";
                }
            }
            return Ok(model);
        }
    }
}
