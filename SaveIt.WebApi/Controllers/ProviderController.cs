﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaveIt.WebApi.Enitity;
using SaveIt.WebApi.Models;

namespace SaveIt.WebApi.Controllers
{
    [RoutePrefix("api/Provider")]
    public class ProviderController : ApiController
    {
        private waferEntities db = new waferEntities();
        string url = "http://www.waffertoday.com";
        // GET: api/Provider
       
        public IHttpActionResult Post(int? AreaId, int? DepartmentId)
        {
            var donator = db.Donators.Where(c => c.IsActive == 1 || c.IsActive == 2)
                 .Where(c => AreaId == null|| AreaId==0 || c.RegionId == AreaId)
                 .Where(c => DepartmentId == null|| DepartmentId==0 || c.CatID ==DepartmentId)
                 .Include(c=>c.WorkCalenders).Include(c => c.FeatureServices).Include(c => c.DonatServices)
                 .Select(c => new ProviderModel {
                     Id=c.ID,
                     Address=c.Address,
                     Features=c.FeatureServices.Select(v=>v.Feature).ToList(),
                     Name=c.OrgName,
                     Notes=c.TradeActivity,
                     PhoneNumber=c.Phone1,
                     Latitude=c.LocatLat,
                     Longitude=c.LocatLang,
                     Photourl= url + c.Photo,
                     WorkCalender=c.WorkCalenders.Select(x=>new WorkModel {
                         workto=x.validto,
                         workfrom=x.ValidFrom
                     }).ToList(),
                     Services=c.DonatServices.Select(x=>new ServiceModel{
                         Code=x.ID,
                         Discount=x.Discount,
                         ServiceName=x.Service
                     }).ToList()
                 }).ToList();
            return Ok(donator);
        }

        // GET: api/Provider/5
        [ResponseType(typeof(Donator))]
        public IHttpActionResult Get(int id)
        {
            Donator c = db.Donators.Include(x => x.WorkCalenders)
                                   .Include(x => x.FeatureServices)
                                   .Include(x => x.DonatServices)
                                   .FirstOrDefault(x=>x.ID==id);
            var provider = new ProviderModel
            {
                Id = c.ID,
                Address = c.Address,
                Features = c.FeatureServices.Select(v => v.Feature).ToList(),
                Name = c.OrgName,
                Notes = c.TradeActivity,
                PhoneNumber = c.Phone1,
                Latitude = c.LocatLat,
                Longitude = c.LocatLang,
                Photourl = url+ c.Photo,
                WorkCalender = c.WorkCalenders.Select(x => new WorkModel
                {
                    workto = x.validto,
                    workfrom = x.ValidFrom
                }).ToList(),
                Services = c.DonatServices.Select(x => new ServiceModel
                {
                    Code = x.ID,
                    Discount = x.Discount,
                    ServiceName = x.Service
                }).ToList()
            };
            return Ok(provider);
        }
    }
}