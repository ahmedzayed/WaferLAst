﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using SaveIt.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SaveIt.WebApi.Controllers
{
    public class ResetPasswordController : ApiController
    {

        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;

        public ResetPasswordController()
        {
        }

        public ResetPasswordController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // POST: api/ResetPassword
        public IHttpActionResult Post([FromBody]ChangePasswordBindingModel reset)
        {
            var usr = UserManager.FindById(User.Identity.GetUserId());
            var model = new RegisterRequest();
            if (UserManager.CheckPassword(usr, reset.OldPassword))
            {
                var token = UserManager.GeneratePasswordResetToken(User.Identity.GetUserId());
                var res = UserManager.ResetPassword(User.Identity.GetUserId(), token, reset.NewPassword);
                if (res.Succeeded)
                {
                    model.Code = 1;
                    model.RequstDetails = "تم تغيير الباسورد بنجاح";
                }
                else
                {
                    model.Code = 0;
                    model.RequstDetails = "حدث خطا في تغيير الباسورد";
                }
            }
            else
            {
                model.Code = 0;
                model.RequstDetails = "الباسورد القديم خطا";
            }
            return Ok(model);
        }
    }
}
