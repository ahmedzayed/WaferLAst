﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using SaveIt.WebApi.Enitity;
using SaveIt.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SaveIt.WebApi.Controllers
{
    public class LoginController : ApiController
    {
        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;

        public LoginController()
        {
        }

        public LoginController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }
        private waferEntities db = new waferEntities();
        public IHttpActionResult Post([FromBody]LoginBindingModel model)
        {
            var request = new UserInfoModel();
            var usr = UserManager.FindByName(model.UserName);
            if (usr == null)
            {
                request.Code = 20;
                request.RequstDetails = "failed username or password";
                return Ok(request);
            }
            var data = "UserName=" + usr.UserName + "&Password=" + model.Password + "&grant_type=password";
            var url = Url.Content("~/token");
            Requests req;
            using (var client = new WebClient())
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                var result = client.UploadString(url, "POST", data);
                req = JsonConvert.DeserializeObject<Requests>(result);
            }
            if (req == null)
            {
                request.Code = 20;
                request.RequstDetails = "failed username or password";
                return Ok(request);
            }
            var card = db.Cards.Find(usr.UserID);
            request.Code = 100;
            request.RequstDetails = "loginSuccess";
            request.FullName = card.Name;
            request.UserName = usr.UserName;
            request.Email = usr.Email;
            request.PhoneNumber = card.Phone;
            request.access_token = req.access_token;
            request.token_type = req.token_type;
            return Ok(request);
        }
    }
}
