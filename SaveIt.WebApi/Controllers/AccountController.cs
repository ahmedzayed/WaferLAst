﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using SaveIt.WebApi.Enitity;
using SaveIt.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SaveIt.WebApi.Controllers
{
    public class AccountController : ApiController
    {
        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }
        private waferEntities db = new waferEntities();
        public IHttpActionResult Post([FromBody]Editprofile model)
        {
            var res = new RegisterRequest();
            var usr = UserManager.FindById(User.Identity.GetUserId());
            if (usr.Type == 4)
            {
                var card = db.Cards.Find(usr.UserID);
                if (card != null)
                {
                    card.Age = model.Age;
                    card.Name = model.Name;
                    card.Phone = model.Phone;
                    card.Address = model.Address;
                    card.workAddress = model.workAddress;
                    card.Villages = model.Villages;
                    db.Cards.AddOrUpdate(card);
                    db.SaveChanges();
                    res.Code = 1;
                  res.RequstDetails = "تم تعديل البيانات بنجاح";
                    return Ok(res);
                }
            }
                res.Code = 0;
                res.RequstDetails = "حدث خطا في تسجيل الدخول";
            return Ok(res);
        }
    }
}
