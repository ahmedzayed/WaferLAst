﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using SaveIt.WebApi.Enitity;
using SaveIt.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SaveIt.WebApi.Controllers
{
    public class RegisterController : ApiController
    {
        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;

        public RegisterController()
        {
        }

        public RegisterController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }
        private waferEntities db = new waferEntities();
        public IHttpActionResult Post([FromBody]RegisterBindingModel model)
        {
            var request = new RegisterRequest();
            try
            {

         
            if (UserManager.FindByEmail(model.Email) != null)
            {
                request.Code = 20;
                request.RequstDetails = "Email Exit";
                return Ok(request);
            }
            if (UserManager.FindByName(model.UserName) != null)
            {
                request.Code = 21;
                request.RequstDetails = "Username Exit";
                return Ok(request);
            }
            var cards = new Card { Name = model.FullName, Email = model.Email, Phone = model.PhoneNumber };
            db.Cards.Add(cards);
            var ind = db.SaveChanges();
            if (ind > 0)
            {
                var usr = new ApplicationUser
                {
                    UserName = model.UserName,
                    Email = model.Email,
                    Type = 4,
                    FullName = model.UserName,
                    UserID = cards.ClientID
                };
                var suc = UserManager.Create(usr, model.Password);
                if (!suc.Succeeded)
                {
                    db.Cards.Remove(cards);
                    db.SaveChanges();

                    request.Code = 22;
                    request.RequstDetails = "Password Shoud Be Atleast 4 chars";
                }
                else
                {
                    var user = UserManager.FindByName(usr.UserName);
                    UserManager.AddToRole(user.Id, "Client");
                    request.Code = 100;
                    request.RequstDetails = "Register Success";
                }
            }
            else
            {
                request.Code = 23;
                request.RequstDetails = "Please Enter all Info";
            }
            }
            catch (Exception)
            {
                request.Code = 23;
                request.RequstDetails = "Please Enter all Info";
            }
            return Ok(request);
        }
    }
}
