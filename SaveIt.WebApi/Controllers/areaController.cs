﻿using SaveIt.WebApi.Enitity;
using SaveIt.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace SaveIt.WebApi.Controllers
{
    public class areaController : ApiController
    {
        
        private waferEntities db = new waferEntities();
        public IHttpActionResult Get()
        {
            var areas = db.Regions.Select(c=>new AreaModel {
                Id=c.ID,
                Name=c.Name,
                CountProvider=c.Donators.Count,
                Photo= Basic.ApiUrl + "/Images/location.png"
            });
            return Ok(areas);
        }
    }
}
