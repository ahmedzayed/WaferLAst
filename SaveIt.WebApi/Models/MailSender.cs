﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mail;

namespace SaveIt.WebApi.Models
{
    public static class MailSender
    {
        public static string GetCode()
        {
            int _min = 100000;
            int _max = 999999;
            Random _rdm = new Random();
            var code = _rdm.Next(_min, _max);
            var msg = Convert.ToString(code);
            return msg;
        }
        public static bool SendMessage(string RecevierMail, string Subject, string Body,string title)
        {
            string message = "<div style='background-color: #e8e8e8;'> <table class='wrapper' cellpadding='0' cellspacing='0' role='presentation' style='text-align:center; width:70%;  margin-right:15%; padding: 32px; border-radius: 5px;   background-color: #fff;'> <tr> <td><div role='section'> <div style='background-color: #e6e6e6;padding: 20px;'> <h1 style='text-align: center; color:#0c4993'><strong>";
            message += Subject;
            message += " </strong> </h1><p style='text-align: center; padding:10px; font-size:18px; color:#1a5b07'>";
            message += Body;
            message += "</p><p style='text-align: right; font-size:16px;'>";
        
            message += " </p>   <p style='text-align: right; font-size:16px;'>";
           
            message += "</p> </div> </div><br> <div role='contentinfo'> <br><div style='text-align:center;'><h3><strong>Waffer Orginzation</strong></h3> </div> </div>  </td>  </tr> </table></div>";
            return SendMail("m70m2018@gmail.com", "smtp.gmail.com", "00112233445566778899", RecevierMail, title, message);
        }
        public static bool SendMail(string SenderMail,string smtp,string Password,string RecevierMail,string Subject,string Body)
        {
            try
            {
                var state = false;
                if (right(SenderMail, 9) == "gmail.com")
                {
                    state = true;
                }
                else if (right(SenderMail, 9) == "yahoo.com")
                {
                    state = false;
                }
                System.Net.Mail.MailAddress From = new System.Net.Mail.MailAddress(SenderMail);
                System.Net.Mail.MailAddress To = new System.Net.Mail.MailAddress(RecevierMail);
                System.Net.Mail.MailMessage Message = new System.Net.Mail.MailMessage(From, To);
                
                Message.Subject = Subject;
                Message.Body = Body;
                Message.IsBodyHtml = true;
                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(smtp);
                client.EnableSsl = Convert.ToBoolean(state);
                client.Credentials = new System.Net.NetworkCredential(SenderMail, Password);
                client.Send(Message);

                return true;
            }
            catch (Exception e) { }
            return false;
        }
        public static string right(string param, int length)
        {
            string result = param.Substring(param.Length - length, length);
            return result;
        }

    }
}