﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaveIt.WebApi.Models
{
    public class AreaModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Photo { get; set; }
        public int CountProvider { get; set; }
    }
}