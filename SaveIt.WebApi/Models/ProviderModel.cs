﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaveIt.WebApi.Models
{
    public class ProviderModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string Notes { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string Photourl { get; set; }
        public List<string> Features{ get; set; }
        public List<ServiceModel> Services { get; set; }
        public List<WorkModel> WorkCalender { get; set; }
    }
    public class WorkModel
    {
        public string workfrom { get; set; }
        public string workto { get; set; }
    }
    public class ServiceModel
    {
        public int Code { get; set; }
        public string ServiceName { get; set; }
        public int?  Discount { get; set; }
    }
    public class SearchProvider
    {
        public int? AreaId { get; set; }
        public int? DepartmentId { get; set; }
    }
}