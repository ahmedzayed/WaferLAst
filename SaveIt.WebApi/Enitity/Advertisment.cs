//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SaveIt.WebApi.Enitity
{
    using System;
    using System.Collections.Generic;
    
    public partial class Advertisment
    {
        public int Id { get; set; }
        public string ClientName { get; set; }
        public string PhotoPath { get; set; }
        public Nullable<System.DateTime> FinishDate { get; set; }
        public string Period { get; set; }
    }
}
